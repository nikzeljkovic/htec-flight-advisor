package com.htec.flightadvisor.data.builder;

import java.util.List;

import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.dto.CommentDto;

public class CityDataBuilder {

  public static CityDto buildCityDto(Long id, String name, String country, String description,
      List<CommentDto> comments) {
    return CityDto.builder().id(id).name(name).country(country).description(description).comments(comments).build();
  }

  public static CityDto buildCityDto(Long id) {
    return CityDto.builder().id(id).build();
  }
}
