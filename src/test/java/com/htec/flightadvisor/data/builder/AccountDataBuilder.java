package com.htec.flightadvisor.data.builder;

import java.util.List;

import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.RoleDto;
import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.entity.Role;

public class AccountDataBuilder {

  public static AccountDto buildAccountDto(Long id) {
    return AccountDto.builder().id(id).build();
  }

  public static AccountDto buildAccountDto(String email) {
    return AccountDto.builder().email(email).build();
  }

  public static Account buildAccount(String email) {
    return Account.builder().email(email).build();
  }

  public static AccountDto buildAccountDto(Long id, String email, String firstName, String lastName) {
    return AccountDto.builder().id(id).email(email).firstName(firstName).lastName(lastName).build();
  }

  public static AccountDto buildAccountDto(String password, String firstName, String lastName) {
    return AccountDto.builder().password(password).firstName(firstName).lastName(lastName).build();
  }

  public static AccountDto buildAccountDto(String password, String firstName, String lastName, List<RoleDto> roles) {
    return AccountDto.builder().password(password).firstName(firstName).lastName(lastName).roles(roles).build();
  }

  public static Account buildAccount(String password, String firstName, String lastName, List<Role> roles) {
    return Account.builder().password(password).firstName(firstName).lastName(lastName).roles(roles).build();
  }

  public static Account buildAccount(Long id) {
    return Account.builder().id(id).build();
  }

  public static AccountDto buildAccountDto(Long id, String email, String password, String firstName, String lastName,
      List<RoleDto> roles) {
    return AccountDto.builder().id(id).email(email).password(password).firstName(firstName).lastName(lastName)
        .roles(roles).build();
  }

  public static Account buildAccount(Long id, String email, String password, String firstName, String lastName,
      List<Role> roles) {
    return Account.builder().id(id).email(email).password(password).firstName(firstName).lastName(lastName).roles(roles)
        .build();
  }
}
