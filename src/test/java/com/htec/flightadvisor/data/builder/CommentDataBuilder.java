package com.htec.flightadvisor.data.builder;

import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.dto.CommentDto;

public class CommentDataBuilder {

  public static CommentDto buildCommentDto(Long id, String description, CityDto cityDto) {
    return CommentDto.builder().id(id).description(description).city(cityDto).build();
  }
}
