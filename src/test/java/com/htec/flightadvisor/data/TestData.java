package com.htec.flightadvisor.data;

public final class TestData {

  public static final String ADMINISTRATOR_EMAIL = "admin@flight.com";
  public static final Long ADMINISTRATOR_ID = 1L;
  public static final Long REGISTERED_USER_ACCOUNT_ID = 2L;
  public static final String ADMINISTRATOR_FIRST_NAME = "System";
  public static final String ADMINISTRATOR_LAST_NAME = "Admin";

  public static final Long ACCOUNT_REGISTRATION_RANDOM_ID = 1996L;
  public static final String ACCOUNT_REGISTRATION_EMAIL = "nikzeljkovic@gmail.com";
  public static final String ACCOUNT_REGISTRATION_INVALID_EMAIL = "not_email.form@t";
  public static final String ACCOUNT_REGISTRATION_PASSWORD = "password";
  public static final String ACCOUNT_REGISTRATION_SHORT_PASSWORD = "short";
  public static final String ACCOUNT_REGISTRATION_FIRST_NAME = "Nikola";
  public static final String ACCOUNT_REGISTRATION_LAST_NAME = "Zeljkovic";
  public static final String DEFAULT_ACCOUNT_ROLE_LABEL = "ROLE_USER";

  public static final String DEFAULT_ENCODED_PASSWORD = "$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.";

  public static final String CITY_NAME = "Novi Sad";
  public static final String CITY_COUNTRY = "Serbia";
  public static final String CITY_DESCRIPTION = "Best city ever!";
  public static final Long CITY_RANDOM_ID = 999L;
  public static final String CITY_EXISTING_NAME = "Belgrade";
  public static final String CITY_EXISTING_NAME_TWO = "Frankfurt";
  public static final Long CITY_EXISTING_ID = 1L;
  public static final Long CITY_NON_EXISTING_ID = 999L;
  public static final String CITY_NON_EXISTING_NAME = "CityRandomName";
  public static final String CITY_WITHOUT_AIRPORTS = "CityWithoutAirports";

  public static final String COMMENT_DESCRIPTION = "A great city!";
  public static final Long EXISTING_COMMENT_ID = 1L;
  public static final Long COMMENT_RANDOM_ID = 999L;
}
