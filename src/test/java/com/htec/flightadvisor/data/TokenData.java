package com.htec.flightadvisor.data;

import java.util.Collections;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public final class TokenData {

  public static HttpHeaders ADMIN_HEADER;

  public static HttpHeaders REGISTERED_USER_HEADER;

  public static HttpHeaders GUEST_JSON_HEADER;

  public static HttpHeaders GUEST_HTML_HEADER;

  static {
    ADMIN_HEADER = new HttpHeaders();
    ADMIN_HEADER.add(HttpHeaders.AUTHORIZATION,
        "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbkBmbGlnaHQuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTklTVFJBVE9SIl0sImp0aSI6IjBiNWEzMDUzLWZlMWQtNGE4My04YWEyLTcxNWY4OTZkN2QxZCIsImNsaWVudF9pZCI6IndlYi1jbGllbnQiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXX0.P1d-irfy4znSOGJbt20IDPbOK1m2j5UKQpoh1Ilg4yqB1ZRx8XofyRZ3LNt4vnyNJaWz2liUCzvOJGxSyLeSTBG_njv1q0Qv_OGbDZ-0WILQISY519UZdDekAD18oNRUajOZ1dpZmSmHMNPnFAbw8XXmwBAGGcYsMchSma9dj-yKJWrUBH110aXXZDfLPFM4LYGRQmVEFTDC7HQ6McCrnJGaHmE5J7mNhF2ZweWKC9jKsp7fSDFHBq9H80kvOsLw85BWIYk94eLJmyXNVhjl2IJpTdPqw5CUs9X3EUw7TwaNZB7PomiUCx1MkM__sYWOF9wyLxyPquUGfbvR3v91LA");
    ADMIN_HEADER.setContentType(MediaType.APPLICATION_JSON);

    REGISTERED_USER_HEADER = new HttpHeaders();
    REGISTERED_USER_HEADER.add(HttpHeaders.AUTHORIZATION,
        "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJyZWdpc3RlcmVkQGZsaWdodC5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiYjlkZmU5YTItZDcyZC00OTNhLWFlMzEtYjA0MDFhNDJjMjVhIiwiY2xpZW50X2lkIjoid2ViLWNsaWVudCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdfQ.VZjofJgd9JNxCNkCOL-cffWhlUqZHNX6efdHpNZop8Jphw9IlibdpkJwn-GXEI8k5ATfhyW9qijmcVwQbCCkGywrYDMsWhWlxmybnVO0XDdvGoo5dCozp4eKeUCTTMLVpZvWWNHi-F_oVaWRT-zaCDJSde_1JsQubqwxUDIr9tNyDSkr8X9uBRLL04Fpnh8lSvq5yjWoZxodAMzw90vzSv2OsPOB6RV1VeFMMp07C7EAuZb6JjqIDucSrhlxjf66EmaWa1HL07y64HPAvxyeHF2NFXW8eYDaEYp2oLquYS6sLWPcY6hLMbW1H874HCI0WicImI7c3IVsflxrVsmZAQ");
    REGISTERED_USER_HEADER.setContentType(MediaType.APPLICATION_JSON);

    GUEST_HTML_HEADER = new HttpHeaders();
    GUEST_HTML_HEADER.setContentType(MediaType.APPLICATION_JSON);
    GUEST_HTML_HEADER.setAccept(Collections.singletonList(MediaType.APPLICATION_XHTML_XML));

    GUEST_JSON_HEADER = new HttpHeaders();
    GUEST_JSON_HEADER.setContentType(MediaType.APPLICATION_JSON);
  }
}
