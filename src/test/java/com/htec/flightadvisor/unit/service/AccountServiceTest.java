package com.htec.flightadvisor.unit.service;

import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_EMAIL;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_FIRST_NAME;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_LAST_NAME;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_PASSWORD;
import static com.htec.flightadvisor.data.TestData.DEFAULT_ENCODED_PASSWORD;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.data.builder.AccountDataBuilder;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.exception.InvalidRequestDataException;
import com.htec.flightadvisor.repository.AccountRepository;
import com.htec.flightadvisor.service.AccountService;
import com.htec.flightadvisor.validator.db.AccountDbValidator;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class AccountServiceTest {

  @Autowired
  private AccountService accountService;

  @MockBean
  private AccountRepository accountRepositoryMock;

  @MockBean
  private ModelMapper modelMapperMock;

  @MockBean
  private AccountDbValidator accountDbValidatorMock;

  @MockBean
  private BCryptPasswordEncoder bCryptPasswordEncoderMock;

  @Test(expected = InvalidRequestDataException.class)
  public void registerShouldThrowBadRequestWithExplanationExceptionWhenUserWithGivenEmailAlreadyExist() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
            .when(accountDbValidatorMock).validateRegistrationRequest(accountDto);

    accountService.register(accountDto);

    verify(accountDbValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(modelMapperMock, times(0)).map(any(AccountDto.class), Account.class);
    verify(bCryptPasswordEncoderMock, times(0)).encode(anyString());
    verify(accountRepositoryMock, times(0)).save(any(Account.class));
  }

  @Test
  public void registerShouldReturnAccountIdAfterSuccessfulRegistration() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);
    Account account = AccountDataBuilder.buildAccount(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);
    Account accountWithId = AccountDataBuilder.buildAccount(1L, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doNothing().when(accountDbValidatorMock).validateRegistrationRequest(accountDto);
    when(modelMapperMock.map(accountDto, Account.class)).thenReturn(account);
    when(bCryptPasswordEncoderMock.encode(ACCOUNT_REGISTRATION_PASSWORD)).thenReturn(DEFAULT_ENCODED_PASSWORD);
    when(accountRepositoryMock.save(any(Account.class))).thenReturn(accountWithId);

    DynamicResponse dynamicResponse = accountService.register(accountDto);

    verify(accountDbValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(modelMapperMock, times(1)).map(accountDto, Account.class);
    verify(bCryptPasswordEncoderMock, times(1)).encode(ACCOUNT_REGISTRATION_PASSWORD);
    verify(accountRepositoryMock, times(1)).save(account);
    assertThat("Account id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
  }
}
