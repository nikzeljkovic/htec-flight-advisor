package com.htec.flightadvisor.unit.controller;

import static com.htec.flightadvisor.data.TestData.ADMINISTRATOR_EMAIL;
import static com.htec.flightadvisor.data.TestData.ADMINISTRATOR_FIRST_NAME;
import static com.htec.flightadvisor.data.TestData.ADMINISTRATOR_ID;
import static com.htec.flightadvisor.data.TestData.ADMINISTRATOR_LAST_NAME;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.data.builder.AccountDataBuilder;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.service.AccountService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class AccountControllerTest {

  @Autowired
  private WebApplicationContext webApplicationContext;

  @MockBean
  private AccountService accountServiceMock;

  private MockMvc mockMvc;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setUp() {
    mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
  }

  @Test
  public void getLoggedUserInfoShouldReturnLoginPageWhenUserIsGuest() throws Exception {
    String url = String.format("%s/me", RestApiEndpoints.ACCOUNT);
    mockMvc.perform(get(url).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isFound());

    verify(accountServiceMock, times(0)).findByEmail(anyString());
  }

  @Test
  @WithMockUser(username = ADMINISTRATOR_EMAIL, roles = "ADMINISTRATOR")
  public void getLoggedUserInfoShouldReturnOkAndAccount() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(ADMINISTRATOR_ID, ADMINISTRATOR_EMAIL,
        ADMINISTRATOR_FIRST_NAME, ADMINISTRATOR_LAST_NAME);

    when(accountServiceMock.findByEmail(ADMINISTRATOR_EMAIL)).thenReturn(accountDto);

    String url = String.format("%s/me", RestApiEndpoints.ACCOUNT);
    MvcResult mvcResult =
        mockMvc.perform(get(url).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk()).andReturn();

    verify(accountServiceMock, times(1)).findByEmail(ADMINISTRATOR_EMAIL);
    andExpectedAccountIsReturned(mvcResult);
  }

  private void andExpectedAccountIsReturned(MvcResult mvcResult) throws IOException {
    AccountDto accountDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), AccountDto.class);

    assertThat("Account Id", accountDto.getId(), is(ADMINISTRATOR_ID));
    assertThat("Account email", accountDto.getEmail(), is(ADMINISTRATOR_EMAIL));
    assertThat("First name", accountDto.getFirstName(), is(ADMINISTRATOR_FIRST_NAME));
    assertThat("Last name", accountDto.getLastName(), is(ADMINISTRATOR_LAST_NAME));
    assertThat("Password", accountDto.getPassword(), isEmptyOrNullString());
  }
}
