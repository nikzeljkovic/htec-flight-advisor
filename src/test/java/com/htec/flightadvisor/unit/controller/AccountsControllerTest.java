package com.htec.flightadvisor.unit.controller;

import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_EMAIL;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_FIRST_NAME;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_INVALID_EMAIL;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_LAST_NAME;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_PASSWORD;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_SHORT_PASSWORD;
import static com.htec.flightadvisor.data.TestData.ADMINISTRATOR_EMAIL;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Collections;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.data.builder.AccountDataBuilder;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.dto.RoleDto;
import com.htec.flightadvisor.exception.InvalidRequestDataException;
import com.htec.flightadvisor.service.AccountService;
import com.htec.flightadvisor.validator.dto.AccountDtoValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class AccountsControllerTest {

  @Autowired
  private WebApplicationContext webApplicationContext;

  @MockBean
  private AccountService accountServiceMock;

  @MockBean
  private AccountDtoValidator accountDtoValidatorMock;

  private MockMvc mockMvc;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setUp() {
    mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenAccountIdIsProvided() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenEmailIsNotProvided() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, ACCOUNT_REGISTRATION_PASSWORD,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenEmailIsEmptyString() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, "", ACCOUNT_REGISTRATION_PASSWORD,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest()).andExpect(
            jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenInvalidEmailFormat() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_INVALID_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenPasswordIsNotProvided() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, null,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenPasswordIsLessThanEightCharacters() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_SHORT_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(
        RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH)))
            .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest()).andExpect(jsonPath("$.data.explanation", is(RestApiErrors
            .fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenFirstNameIsNull() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, null, ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenFirstNameIsEmptyString() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, "", ACCOUNT_REGISTRATION_LAST_NAME, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest()).andExpect(
            jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenLastNameIsNull() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, null, null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenLastNameIsEmptyString() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, "", null);

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest()).andExpect(
            jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenRolesIsNotEmptyCollection() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME,
        Collections.singletonList(RoleDto.builder().id(1L).build()));

    doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES)))
        .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
    verify(accountServiceMock, times(0)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnBadRequestWhenAccountWithGivenEmailAlreadyExists() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(ADMINISTRATOR_EMAIL);

    doNothing().when(accountDtoValidatorMock).validateRegistrationRequest(any(AccountDto.class));
    doThrow(new InvalidRequestDataException(
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
            .when(accountServiceMock).register(any(AccountDto.class));

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest()).andExpect(jsonPath("$.data.explanation",
            is(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL))));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(any(AccountDto.class));
    verify(accountServiceMock, times(1)).register(any(AccountDto.class));
  }

  @Test
  public void registerAccountShouldReturnCreatedWhenUserIsSuccessfullyRegistered() throws Exception {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);
    DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 5L);

    doNothing().when(accountDtoValidatorMock).validateRegistrationRequest(any(AccountDto.class));
    when(accountServiceMock.register(any(AccountDto.class))).thenReturn(dynamicResponse);

    mockMvc
        .perform(post(RestApiEndpoints.ACCOUNTS).content(objectMapper.writeValueAsString(accountDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isCreated()).andExpect(jsonPath("$.id", is(5)));

    verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(any(AccountDto.class));
    verify(accountServiceMock, times(1)).register(any(AccountDto.class));
  }
}
