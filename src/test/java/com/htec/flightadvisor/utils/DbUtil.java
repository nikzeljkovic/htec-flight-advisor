package com.htec.flightadvisor.utils;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.entity.Comment;
import com.htec.flightadvisor.repository.AccountRepository;
import com.htec.flightadvisor.repository.CityRepository;
import com.htec.flightadvisor.repository.CommentsRepository;

@Service
public class DbUtil {

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private CityRepository cityRepository;

  @Autowired
  private CommentsRepository commentsRepository;

  public Account findAccountByEmail(String accountEmail) {
    return accountRepository.findByEmail(accountEmail).orElse(null);
  }

  public Optional<Account> findOneOptional(String accountEmail) {
    return accountRepository.findByEmail(accountEmail);
  }

  public Account findAccountById(Long accountId) {
    return accountRepository.findById(accountId).orElse(null);
  }

  public City findCityById(Long cityId) {
    return cityRepository.findById(cityId).orElse(null);
  }

  public Comment findCommentById(Long commentId) {
    return commentsRepository.findById(commentId).orElse(null);
  }
}
