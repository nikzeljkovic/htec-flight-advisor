package com.htec.flightadvisor.utils;

import static junit.framework.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.exception.ApiError;

public class CommonFunctions {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static void thenOkIsReturned(ResponseEntity responseEntity) {
    assertThat("Http status code is correct.", responseEntity.getStatusCode(), is(HttpStatus.OK));
  }

  public static void thenOkIsReturned(ResponseEntity responseEntity, String message) {
    assertThat("Http status code is correct.", responseEntity.getStatusCode(), is(HttpStatus.OK));
    validateMessage(message, responseEntity);
  }

  public static void thenCreatedIsReturned(ResponseEntity responseEntity) {
    assertThat("Http status code is correct.", responseEntity.getStatusCode(), is(HttpStatus.CREATED));
  }

  public static void thenBadRequestIsReturned(ResponseEntity responseEntity, String error) {
    assertThat("Http status code is correct.", responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    validateError(error, responseEntity);
  }

  public static void thenForbiddenIsReturned(ResponseEntity responseEntity) {
    assertThat("Http status code is correct.", responseEntity.getStatusCode(), is(HttpStatus.FORBIDDEN));
  }

  public static void thenNotFoundIsReturned(ResponseEntity responseEntity, String error) {
    assertThat("Http status code is correct.", responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    validateError(error, responseEntity);
  }

  public static void thenNoContentIsReturned(ResponseEntity responseEntity) {
    assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.NO_CONTENT));
  }

  private static void validateMessage(String message, ResponseEntity responseEntity) {
    try {
      DynamicResponse dynamicResponse =
          objectMapper.readValue(responseEntity.getBody().toString(), DynamicResponse.class);
      assertThat("Message is correct.", message, is(dynamicResponse.get(RestApiConstants.MESSAGE)));

    } catch (IOException e) {
      e.printStackTrace();
      fail("Unexpected exception, test should fail.");
    }
  }

  private static void validateError(String explanationErrorMessage, ResponseEntity responseEntity) {
    if (responseEntity.getBody() instanceof DynamicResponse) {
      processDynamicResponseErrorValidation(explanationErrorMessage, (DynamicResponse) responseEntity.getBody());
    } else {
      try {
        ApiError apiError = objectMapper.readValue(responseEntity.getBody().toString(), ApiError.class);
        assertThat("Error message is correct.", apiError.getData().get(RestApiConstants.EXPLANATION),
            is(explanationErrorMessage));
      } catch (IOException e) {
        e.printStackTrace();
        fail("Unexpected exception, test should fail.");
      }
    }
  }

  private static void processDynamicResponseErrorValidation(String error, DynamicResponse dynamicResponse) {
    Map<String, String> data = (Map<String, String>) dynamicResponse.get(RestApiConstants.DATA);

    if (data != null) {
      assertThat("Error message is correct.", data.get(RestApiConstants.EXPLANATION), is(error));
    }
  }

  public static Long andEntityIdIsReturned(DynamicResponse body) {
    assertThat(RestApiConstants.ID + " is not null.", body.get(RestApiConstants.ID), notNullValue());

    return ((Integer) body.get(RestApiConstants.ID)).longValue();
  }

  public static void thenLoginPageIsReturned(ResponseEntity responseEntity) {
    boolean correctStatusCode =
        responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.FOUND;
    assertThat("Http status code is correct.", correctStatusCode);

    if (responseEntity.getStatusCode() == HttpStatus.OK) {
      assertThat("Content Type is correct.", responseEntity.getHeaders().get("Content-Type").get(0).contains("html"));
    } else {
      assertThat("Redirect URI is correct.", responseEntity.getHeaders().getLocation().getPath().contains("login"));
    }
  }

  public static void then(ResponseEntity<DynamicResponse> responseEntity, String fieldInsideFieldShouldNotBeNull) {
  }
}
