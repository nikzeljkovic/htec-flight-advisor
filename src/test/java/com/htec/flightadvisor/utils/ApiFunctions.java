package com.htec.flightadvisor.utils;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.common.api.RestApiRequestParams;
import com.htec.flightadvisor.data.model.RestResponsePage;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.dto.DynamicResponse;

public class ApiFunctions {

  public static ResponseEntity<DynamicResponse> registerAccount(TestRestTemplate testRestTemplate,
      AccountDto accountDto) {
    String url = String.format("%s", RestApiEndpoints.ACCOUNTS);
    return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(accountDto), DynamicResponse.class);
  }

  public static ResponseEntity<DynamicResponse> createCity(TestRestTemplate testRestTemplate, HttpHeaders headers,
      CityDto cityDto) {
    String url = String.format("%s", RestApiEndpoints.CITIES);
    return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(cityDto, headers), DynamicResponse.class);
  }

  public static ResponseEntity<DynamicResponse> createComment(TestRestTemplate testRestTemplate, HttpHeaders headers,
      CommentDto commentDto) {
    String url = String.format("%s", RestApiEndpoints.COMMENTS);
    return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(commentDto, headers),
        DynamicResponse.class);
  }

  public static ResponseEntity deleteCommentById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
    String url = String.format("%s/%d", RestApiEndpoints.COMMENT, id);
    return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
  }

  public static ResponseEntity updateComment(TestRestTemplate testRestTemplate, HttpHeaders header,
      CommentDto commentDto) {
    String url = String.format("%s", RestApiEndpoints.COMMENT);
    return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(commentDto, header), String.class);
  }

  public static ResponseEntity findShortestPath(TestRestTemplate testRestTemplate, HttpHeaders headers,
      String sourceCityName, String destinationCityName) {
    String url = String.format("%s?%s=%s&%s=%s", RestApiEndpoints.ROUTES, RestApiRequestParams.SOURCE_CITY_NAME,
        sourceCityName, RestApiRequestParams.DESTINATION_CITY_NAME, destinationCityName);
    return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
  }

  public static ResponseEntity<RestResponsePage<CityDto>> findAllCities(TestRestTemplate testRestTemplate,
      HttpHeaders header, String cityName, Integer commentsLimit) {
    String url = String.format("%s?%s=%s&%s=%s", RestApiEndpoints.CITIES, RestApiRequestParams.NAME, cityName,
        RestApiRequestParams.COMMENTS_LIMIT, commentsLimit);
    return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(header),
        new ParameterizedTypeReference<RestResponsePage<CityDto>>() {
        });
  }
}
