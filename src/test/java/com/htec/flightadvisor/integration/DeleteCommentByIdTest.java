package com.htec.flightadvisor.integration;

import static com.htec.flightadvisor.data.TestData.COMMENT_RANDOM_ID;
import static com.htec.flightadvisor.data.TestData.EXISTING_COMMENT_ID;
import static com.htec.flightadvisor.data.TokenData.ADMIN_HEADER;
import static com.htec.flightadvisor.data.TokenData.GUEST_HTML_HEADER;
import static com.htec.flightadvisor.data.TokenData.REGISTERED_USER_HEADER;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.entity.Comment;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;
import com.htec.flightadvisor.utils.DbUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")
public class DeleteCommentByIdTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Autowired
  private DbUtil dbUtil;

  @Test
  public void shouldReturnLoginPageWhenUserIsGuest() {
    ResponseEntity responseEntity =
        ApiFunctions.deleteCommentById(testRestTemplate, GUEST_HTML_HEADER, COMMENT_RANDOM_ID);

    CommonFunctions.thenLoginPageIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnNotFoundWhenDeletingNonExistingComment() {
    ResponseEntity responseEntity = ApiFunctions.deleteCommentById(testRestTemplate, ADMIN_HEADER, COMMENT_RANDOM_ID);

    CommonFunctions.thenNotFoundIsReturned(responseEntity,
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.COMMENT, RestApiConstants.ID));
  }

  @Test
  public void shouldReturnForbiddenWhenDeletingOtherUsersComment() {
    ResponseEntity responseEntity =
        ApiFunctions.deleteCommentById(testRestTemplate, REGISTERED_USER_HEADER, EXISTING_COMMENT_ID);

    CommonFunctions.thenForbiddenIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnNoContentAndDeleteComment() {
    ResponseEntity responseEntity = ApiFunctions.deleteCommentById(testRestTemplate, ADMIN_HEADER, EXISTING_COMMENT_ID);

    CommonFunctions.thenNoContentIsReturned(responseEntity);
    andCommentIsDeleted(EXISTING_COMMENT_ID);
  }

  private void andCommentIsDeleted(Long commentId) {
    Comment comment = dbUtil.findCommentById(commentId);

    assertThat("Comment is deleted.", comment, is(nullValue()));
  }
}
