package com.htec.flightadvisor.integration;

import static com.htec.flightadvisor.data.TestData.CITY_COUNTRY;
import static com.htec.flightadvisor.data.TestData.CITY_DESCRIPTION;
import static com.htec.flightadvisor.data.TestData.CITY_EXISTING_NAME;
import static com.htec.flightadvisor.data.TestData.CITY_NAME;
import static com.htec.flightadvisor.data.TestData.CITY_RANDOM_ID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.data.TokenData;
import com.htec.flightadvisor.data.builder.CityDataBuilder;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;
import com.htec.flightadvisor.utils.DbUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")
public class CreateCityTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Autowired
  private DbUtil dbUtil;

  @Test
  public void shouldReturnLoginPageWhenUserIsGuest() {
    CityDto cityDto = CityDataBuilder.buildCityDto(CITY_RANDOM_ID, CITY_NAME, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.GUEST_HTML_HEADER, cityDto);

    CommonFunctions.thenLoginPageIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnForbiddenWhenUserIsNotAdmin() {
    CityDto cityDto = CityDataBuilder.buildCityDto(CITY_RANDOM_ID, CITY_NAME, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.REGISTERED_USER_HEADER, cityDto);

    CommonFunctions.thenForbiddenIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnBadRequestWhenIdIsNotNull() {
    CityDto cityDto = CityDataBuilder.buildCityDto(CITY_RANDOM_ID, CITY_NAME, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
  }

  @Test
  public void shouldReturnBadRequestWhenNameIsNull() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, null, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenNameIsEmptyString() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, StringUtils.EMPTY, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenCountryIsNull() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_NAME, null, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.COUNTRY));
  }

  @Test
  public void shouldReturnBadRequestWhenCountryIsEmptyString() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_NAME, StringUtils.EMPTY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.COUNTRY));
  }

  @Test
  public void shouldReturnBadRequestWhenDescriptionIsNull() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_NAME, CITY_COUNTRY, null, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DESCRIPTION));
  }

  @Test
  public void shouldReturnBadRequestWhenDescriptionIsEmptyString() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_NAME, CITY_COUNTRY, StringUtils.EMPTY, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.DESCRIPTION));
  }

  @Test
  public void shouldReturnBadRequestWhenCommentsAreNotNullAndNotEmptyCollection() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_NAME, CITY_COUNTRY, CITY_DESCRIPTION,
        Collections.singletonList(new CommentDto()));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.COMMENTS));
  }

  @Test
  public void shouldReturnBadRequestWhenCityExistsWithGivenName() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_EXISTING_NAME, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.CITY, RestApiConstants.NAME));
  }

  @Test
  public void shouldReturnCreatedAndCityIdAfterSuccessfulCreation() {
    CityDto cityDto = CityDataBuilder.buildCityDto(null, CITY_NAME, CITY_COUNTRY, CITY_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createCity(testRestTemplate, TokenData.ADMIN_HEADER, cityDto);

    CommonFunctions.thenCreatedIsReturned(responseEntity);
    Long cityId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
    andCityIsStoredWithGivenData(cityId);
  }

  private void andCityIsStoredWithGivenData(Long cityId) {
    City city = dbUtil.findCityById(cityId);

    assertThat("Name is correct.", city.getName(), is(CITY_NAME));
    assertThat("Country is correct.", city.getCountry(), is(CITY_COUNTRY));
    assertThat("Description is correct.", city.getDescription(), is(CITY_DESCRIPTION));
  }
}
