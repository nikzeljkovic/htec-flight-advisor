package com.htec.flightadvisor.integration;

import static com.htec.flightadvisor.data.TestData.COMMENT_DESCRIPTION;
import static com.htec.flightadvisor.data.TestData.COMMENT_RANDOM_ID;
import static com.htec.flightadvisor.data.TestData.EXISTING_COMMENT_ID;
import static com.htec.flightadvisor.data.TokenData.ADMIN_HEADER;
import static com.htec.flightadvisor.data.TokenData.GUEST_HTML_HEADER;
import static com.htec.flightadvisor.data.TokenData.REGISTERED_USER_HEADER;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.data.builder.CommentDataBuilder;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.entity.Comment;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;
import com.htec.flightadvisor.utils.DbUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")
public class UpdateCommentTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Autowired
  private DbUtil dbUtil;

  @Test
  public void shouldReturnLoginPageWhenUserIsGuest() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(EXISTING_COMMENT_ID, COMMENT_DESCRIPTION, null);

    ResponseEntity responseEntity = ApiFunctions.updateComment(testRestTemplate, GUEST_HTML_HEADER, commentDto);

    CommonFunctions.thenLoginPageIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnBadRequestWhenDescriptionIdIsNull() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(null, COMMENT_DESCRIPTION, null);

    ResponseEntity responseEntity = ApiFunctions.updateComment(testRestTemplate, ADMIN_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
  }

  @Test
  public void shouldReturnBadRequestWhenDescriptionIsEmptyString() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(EXISTING_COMMENT_ID, StringUtils.EMPTY, null);

    ResponseEntity responseEntity = ApiFunctions.updateComment(testRestTemplate, ADMIN_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.DESCRIPTION));
  }

  @Test
  public void shouldReturnNotFoundWhenCommentDoesNotExist() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(COMMENT_RANDOM_ID, COMMENT_DESCRIPTION, null);

    ResponseEntity responseEntity = ApiFunctions.updateComment(testRestTemplate, ADMIN_HEADER, commentDto);

    CommonFunctions.thenNotFoundIsReturned(responseEntity,
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.COMMENT, RestApiConstants.ID));
  }

  @Test
  public void shouldReturnBadRequestWhenUpdatingOtherUsersComment() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(EXISTING_COMMENT_ID, COMMENT_DESCRIPTION, null);

    ResponseEntity responseEntity = ApiFunctions.updateComment(testRestTemplate, REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenForbiddenIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnNoContentAndUpdateComment() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(EXISTING_COMMENT_ID, COMMENT_DESCRIPTION, null);

    ResponseEntity responseEntity = ApiFunctions.updateComment(testRestTemplate, ADMIN_HEADER, commentDto);

    CommonFunctions.thenNoContentIsReturned(responseEntity);
    andCommentIsUpdated(EXISTING_COMMENT_ID);
  }

  private void andCommentIsUpdated(Long commentId) {
    Comment comment = dbUtil.findCommentById(commentId);

    assertThat("Description is updated.", comment.getDescription(), is(COMMENT_DESCRIPTION));
    assertThat("Updated at is updated.", comment.getUpdatedAt(), Matchers.is(notNullValue()));
  }
}
