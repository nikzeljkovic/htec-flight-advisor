package com.htec.flightadvisor.integration;

import static com.htec.flightadvisor.data.TestData.CITY_EXISTING_NAME;
import static com.htec.flightadvisor.data.TestData.CITY_EXISTING_NAME_TWO;
import static com.htec.flightadvisor.data.TestData.CITY_NON_EXISTING_NAME;
import static com.htec.flightadvisor.data.TestData.CITY_WITHOUT_AIRPORTS;
import static com.htec.flightadvisor.data.TokenData.REGISTERED_USER_HEADER;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.dto.FlightPlan;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")
public class FindShortestPathTest {

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Test
  public void shouldReturnNotFoundWhenSourceCityDoesNotExist() {
    ResponseEntity responseEntity = ApiFunctions.findShortestPath(testRestTemplate, REGISTERED_USER_HEADER,
        CITY_NON_EXISTING_NAME, CITY_EXISTING_NAME);

    CommonFunctions.thenNotFoundIsReturned(responseEntity,
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY, RestApiConstants.NAME));
  }

  @Test
  public void shouldReturnNotFoundWhenDestinationCityDoesNotExist() {
    ResponseEntity responseEntity = ApiFunctions.findShortestPath(testRestTemplate, REGISTERED_USER_HEADER,
        CITY_EXISTING_NAME, CITY_NON_EXISTING_NAME);

    CommonFunctions.thenNotFoundIsReturned(responseEntity,
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY, RestApiConstants.NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenSourceCityDoesNotHaveAirports() {
    ResponseEntity responseEntity = ApiFunctions.findShortestPath(testRestTemplate, REGISTERED_USER_HEADER,
        CITY_WITHOUT_AIRPORTS, CITY_EXISTING_NAME);

    CommonFunctions.thenOkIsReturned(responseEntity, RestApiErrors.NO_PATH_BETWEEN_CITIES);
  }

  @Test
  public void shouldReturnBadRequestWhenDestinationCityDoesNotHaveAirports() {
    ResponseEntity responseEntity = ApiFunctions.findShortestPath(testRestTemplate, REGISTERED_USER_HEADER,
        CITY_EXISTING_NAME, CITY_WITHOUT_AIRPORTS);

    CommonFunctions.thenOkIsReturned(responseEntity, RestApiErrors.NO_PATH_BETWEEN_CITIES);
  }

  @Test
  public void shouldReturnOkAndShortestPath() throws IOException {
    ResponseEntity responseEntity = ApiFunctions.findShortestPath(testRestTemplate, REGISTERED_USER_HEADER,
        CITY_EXISTING_NAME, CITY_EXISTING_NAME_TWO);

    CommonFunctions.thenOkIsReturned(responseEntity);
    andShortestPathIsReturned(responseEntity);
  }

  private void andShortestPathIsReturned(ResponseEntity responseEntity) throws IOException {
    FlightPlan flightPlan = objectMapper.readValue(responseEntity.getBody().toString(), FlightPlan.class);

    assertThat("Cost is correct", flightPlan.getCost(), closeTo(51.37, 0.1));
    assertThat("Length is correct", flightPlan.getLength(), closeTo(1198.59, 0.1));
    assertThat("Path size is correct", flightPlan.getFlights().size(), is(2));
    assertThat("First path node is correct.", flightPlan.getFlights().get(0).getDestinationAirportId(), is(1613L));
    assertThat("Second path node is correct.", flightPlan.getFlights().get(1).getDestinationAirportId(), is(340L));
  }
}
