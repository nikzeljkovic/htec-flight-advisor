package com.htec.flightadvisor.integration;

import static com.htec.flightadvisor.data.TestData.CITY_EXISTING_ID;
import static com.htec.flightadvisor.data.TestData.CITY_NON_EXISTING_ID;
import static com.htec.flightadvisor.data.TestData.COMMENT_DESCRIPTION;
import static com.htec.flightadvisor.data.TestData.COMMENT_RANDOM_ID;
import static com.htec.flightadvisor.data.TestData.REGISTERED_USER_ACCOUNT_ID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.data.TokenData;
import com.htec.flightadvisor.data.builder.CityDataBuilder;
import com.htec.flightadvisor.data.builder.CommentDataBuilder;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.entity.Comment;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;
import com.htec.flightadvisor.utils.DbUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")
public class CreateCommentTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Autowired
  private DbUtil dbUtil;

  @Test
  public void shouldReturnLoginPageWhenUserIsGuest() {
    CommentDto commentDto =
        CommentDataBuilder.buildCommentDto(null, COMMENT_DESCRIPTION, CityDataBuilder.buildCityDto(CITY_EXISTING_ID));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.GUEST_HTML_HEADER, commentDto);

    CommonFunctions.thenLoginPageIsReturned(responseEntity);
  }

  @Test
  public void shouldReturnBadRequestWhenIdIsNotNull() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(COMMENT_RANDOM_ID, COMMENT_DESCRIPTION,
        CityDataBuilder.buildCityDto(CITY_EXISTING_ID));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
  }

  @Test
  public void shouldReturnBadRequestWhenDescriptionIsNull() {
    CommentDto commentDto =
        CommentDataBuilder.buildCommentDto(null, null, CityDataBuilder.buildCityDto(CITY_EXISTING_ID));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DESCRIPTION));
  }

  @Test
  public void shouldReturnBadRequestWhenDescriptionIsEmptyString() {
    CommentDto commentDto =
        CommentDataBuilder.buildCommentDto(null, StringUtils.EMPTY, CityDataBuilder.buildCityDto(CITY_EXISTING_ID));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.DESCRIPTION));
  }

  @Test
  public void shouldReturnBadRequestWhenCityIsNull() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(null, COMMENT_DESCRIPTION, null);

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY));
  }

  @Test
  public void shouldReturnBadRequestWhenCityIdIsNull() {
    CommentDto commentDto =
        CommentDataBuilder.buildCommentDto(null, COMMENT_DESCRIPTION, CityDataBuilder.buildCityDto(null));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY));
  }

  @Test
  public void shouldReturnBadRequestWhenCityDoesNotExist() {
    CommentDto commentDto = CommentDataBuilder.buildCommentDto(null, COMMENT_DESCRIPTION,
        CityDataBuilder.buildCityDto(CITY_NON_EXISTING_ID));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.then(responseEntity,
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY, RestApiConstants.ID));
  }

  @Test
  public void shouldReturnCreatedAndCommentIdAfterSuccessfulCreation() {
    CommentDto commentDto =
        CommentDataBuilder.buildCommentDto(null, COMMENT_DESCRIPTION, CityDataBuilder.buildCityDto(CITY_EXISTING_ID));

    ResponseEntity<DynamicResponse> responseEntity =
        ApiFunctions.createComment(testRestTemplate, TokenData.REGISTERED_USER_HEADER, commentDto);

    CommonFunctions.thenCreatedIsReturned(responseEntity);
    Long commentId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
    andCommentIsStoredWithGivenData(commentId);
  }

  private void andCommentIsStoredWithGivenData(Long commentId) {
    Comment comment = dbUtil.findCommentById(commentId);

    assertThat("Description is correct.", comment.getDescription(), is(COMMENT_DESCRIPTION));
    assertThat("Account is correct.", comment.getAccount().getId(), is(REGISTERED_USER_ACCOUNT_ID));
    assertThat("City is correct.", comment.getCity().getId(), is(CITY_EXISTING_ID));
    assertThat("Created at is set.", comment.getCreatedAt(), is(notNullValue()));
    assertThat("Update at is not set.", comment.getUpdatedAt(), is(nullValue()));
  }
}
