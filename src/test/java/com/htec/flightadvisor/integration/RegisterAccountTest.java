package com.htec.flightadvisor.integration;

import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_EMAIL;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_FIRST_NAME;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_INVALID_EMAIL;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_LAST_NAME;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_PASSWORD;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_RANDOM_ID;
import static com.htec.flightadvisor.data.TestData.ACCOUNT_REGISTRATION_SHORT_PASSWORD;
import static com.htec.flightadvisor.data.TestData.ADMINISTRATOR_EMAIL;
import static com.htec.flightadvisor.data.TestData.DEFAULT_ACCOUNT_ROLE_LABEL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.data.builder.AccountDataBuilder;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.dto.RoleDto;
import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;
import com.htec.flightadvisor.utils.DbUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")
public class RegisterAccountTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Autowired
  private DbUtil dbUtil;

  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Test
  public void shouldReturnBadRequestWhenAccountIdIsProvided() {
    AccountDto accountDto =
        AccountDataBuilder.buildAccountDto(ACCOUNT_REGISTRATION_RANDOM_ID, ACCOUNT_REGISTRATION_EMAIL,
            ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
  }

  @Test
  public void shouldReturnBadRequestWhenEmailIsNotProvided() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, ACCOUNT_REGISTRATION_PASSWORD,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL));
  }

  @Test
  public void shouldReturnBadRequestWhenEmailIsEmptyString() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, StringUtils.EMPTY, ACCOUNT_REGISTRATION_PASSWORD,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL));
  }

  @Test
  public void shouldReturnBadRequestWhenInvalidEmailFormat() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_INVALID_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL));
  }

  @Test
  public void shouldReturnBadRequestWhenPasswordIsNotProvided() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, null,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD));
  }

  @Test
  public void shouldReturnBadRequestWhenPasswordIsLessThanEightCharacters() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_SHORT_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH));
  }

  @Test
  public void shouldReturnBadRequestWhenFirstNameIsNull() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, null, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenFirstNameIsEmptyString() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, "", ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenLastNameIsNull() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, null, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenLastNameIsEmptyString() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, "", null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));
  }

  @Test
  public void shouldReturnBadRequestWhenRolesIsNotEmptyCollection() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME,
        Collections.singletonList(RoleDto.builder().id(1L).build()));

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES));
  }

  @Test
  public void shouldReturnBadRequestWhenAccountWithGivenEmailAlreadyExists() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ADMINISTRATOR_EMAIL, ACCOUNT_REGISTRATION_PASSWORD,
        ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenBadRequestIsReturned(responseEntity,
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL));
  }

  @Test
  public void shouldReturnCreatedAndAccountIdAfterSuccessfulRegistration() {
    AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL,
        ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null);

    ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

    CommonFunctions.thenCreatedIsReturned(responseEntity);
    Long registeredAccountId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
    andAccountIsStoredWithGivenData(registeredAccountId);
  }

  private void andAccountIsStoredWithGivenData(Long registeredAccountId) {
    Account account = dbUtil.findAccountById(registeredAccountId);

    assertThat("Email is correct.", account.getEmail(), is(ACCOUNT_REGISTRATION_EMAIL));
    assertTrue("Password is correct.",
        bCryptPasswordEncoder.matches(ACCOUNT_REGISTRATION_PASSWORD, account.getPassword()));
    assertThat("First name is correct.", account.getFirstName(), is(ACCOUNT_REGISTRATION_FIRST_NAME));
    assertThat("Last name is correct.", account.getLastName(), is(ACCOUNT_REGISTRATION_LAST_NAME));
    assertThat("Default role is added.", account.getRoles().get(0).getLabel(), is(DEFAULT_ACCOUNT_ROLE_LABEL));
  }
}
