package com.htec.flightadvisor.integration;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htec.flightadvisor.data.TokenData;
import com.htec.flightadvisor.data.model.RestResponsePage;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.utils.ApiFunctions;
import com.htec.flightadvisor.utils.CommonFunctions;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/sql/test-data.sql")

public class FindCitiesTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Test
  public void shouldReturnAllCities() {
    ResponseEntity<RestResponsePage<CityDto>> responseEntity =
        ApiFunctions.findAllCities(testRestTemplate, TokenData.REGISTERED_USER_HEADER, "belgrade", 2);

    CommonFunctions.thenOkIsReturned(responseEntity);
    andCitiesAreReturned(responseEntity);
  }

  private void andCitiesAreReturned(ResponseEntity<RestResponsePage<CityDto>> responseEntity) {
    List<CityDto> cities = responseEntity.getBody().getContent();

    assertThat("Correct city count is returned.", cities.size(), is(1));
    assertThat("Correct comment count is returned.", cities.get(0).getComments().size(), is(2));
  }
}
