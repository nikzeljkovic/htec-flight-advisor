package com.htec.flightadvisor.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.service.CommentsService;
import com.htec.flightadvisor.validator.dto.CommentDtoValidator;

@RestController
@RequestMapping(RestApiEndpoints.COMMENT)
public class CommentController {

  private final CommentsService commentsService;

  private final CommentDtoValidator commentDtoValidator;

  public CommentController(CommentsService commentsService, CommentDtoValidator commentDtoValidator) {
    this.commentsService = commentsService;
    this.commentDtoValidator = commentDtoValidator;
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity deleteById(@PathVariable Long id, Principal principal) {
    commentsService.deleteById(id, principal.getName());

    return ResponseEntity.noContent().build();
  }

  @PutMapping(consumes = APPLICATION_JSON_VALUE)
  public ResponseEntity update(@RequestBody CommentDto commentDto, Principal principal) {
    commentDtoValidator.validateUpdateRequest(commentDto);
    commentsService.update(commentDto, principal.getName());

    return ResponseEntity.noContent().build();
  }
}
