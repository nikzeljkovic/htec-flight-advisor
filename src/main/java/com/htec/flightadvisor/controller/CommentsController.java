package com.htec.flightadvisor.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.common.utils.RestApiUtils;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.service.CommentsService;
import com.htec.flightadvisor.validator.dto.CommentDtoValidator;

@RestController
@RequestMapping(RestApiEndpoints.COMMENTS)
public class CommentsController {

  private final CommentsService commentsService;

  private final CommentDtoValidator commentDtoValidator;

  public CommentsController(CommentsService commentsService, CommentDtoValidator commentDtoValidator) {
    this.commentsService = commentsService;
    this.commentDtoValidator = commentDtoValidator;
  }

  @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
  public ResponseEntity<DynamicResponse> create(@RequestBody CommentDto commentDto, Principal principal) {
    commentDtoValidator.validateCreateRequest(commentDto);

    DynamicResponse body = commentsService.create(commentDto, principal.getName());

    return RestApiUtils.buildCreatedEntityResponse(RestApiEndpoints.COMMENT, body);
  }
}
