package com.htec.flightadvisor.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.common.utils.RestApiUtils;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.service.AccountService;
import com.htec.flightadvisor.validator.dto.AccountDtoValidator;

@RestController
@RequestMapping(RestApiEndpoints.ACCOUNTS)
public class AccountsController {

  private final AccountService accountService;

  private final AccountDtoValidator accountDtoValidator;

  public AccountsController(AccountService accountService, AccountDtoValidator accountDtoValidator) {
    this.accountService = accountService;
    this.accountDtoValidator = accountDtoValidator;
  }

  @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
  public ResponseEntity<DynamicResponse> register(@RequestBody AccountDto accountDto) {
    accountDtoValidator.validateRegistrationRequest(accountDto);
    DynamicResponse body = accountService.register(accountDto);

    return RestApiUtils.buildCreatedEntityResponse(RestApiEndpoints.ACCOUNT, body);
  }
}
