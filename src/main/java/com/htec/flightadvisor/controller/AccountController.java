package com.htec.flightadvisor.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.service.AccountService;

@RestController
@RequestMapping(RestApiEndpoints.ACCOUNT)
public class AccountController {

  private final AccountService accountService;

  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/me")
  public ResponseEntity<AccountDto> getLoggedUserInfo(Principal principal) {
    AccountDto body = accountService.findByEmail(principal.getName());

    return ResponseEntity.ok(body);
  }
}
