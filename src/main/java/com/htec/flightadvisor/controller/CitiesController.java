package com.htec.flightadvisor.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.common.api.RestApiRequestParams;
import com.htec.flightadvisor.common.utils.RestApiUtils;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.service.CityService;
import com.htec.flightadvisor.validator.dto.CityDtoValidator;

@RestController
@RequestMapping(RestApiEndpoints.CITIES)
public class CitiesController {

  private final CityService cityService;

  private final CityDtoValidator cityDtoValidator;

  public CitiesController(CityService cityService, CityDtoValidator cityDtoValidator) {
    this.cityService = cityService;
    this.cityDtoValidator = cityDtoValidator;
  }

  @PreAuthorize("hasAnyAuthority('ROLE_ADMINISTRATOR')")
  @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
  public ResponseEntity<DynamicResponse> create(@RequestBody CityDto cityDto) {
    cityDtoValidator.validateCreateRequest(cityDto);
    DynamicResponse body = cityService.create(cityDto);

    return RestApiUtils.buildCreatedEntityResponse(RestApiEndpoints.CITY, body);
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<CityDto>> findAll(
      @RequestParam(value = RestApiRequestParams.NAME, required = false, defaultValue = StringUtils.EMPTY) String name,
      @RequestParam(value = RestApiRequestParams.COMMENTS_LIMIT, required = false) Integer commentsLimit,
      Pageable pageable) {
    Page<CityDto> cities = cityService.findAll(name, commentsLimit, pageable);

    return ResponseEntity.ok(cities);
  }
}
