package com.htec.flightadvisor.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import org.assertj.core.util.Lists;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htec.flightadvisor.algorithm.CheapestPathFinder;
import com.htec.flightadvisor.common.api.RestApiEndpoints;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.api.RestApiRequestParams;
import com.htec.flightadvisor.common.utils.BadRequestUtils;
import com.htec.flightadvisor.dto.FlightPlan;

@RestController
@RequestMapping(RestApiEndpoints.ROUTES)
public class RoutesController {

  private final CheapestPathFinder cheapestPathFinder;

  public RoutesController(CheapestPathFinder cheapestPathFinder) {
    this.cheapestPathFinder = cheapestPathFinder;
  }

  @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_ADMINISTRATOR')")
  @GetMapping(
      params = { RestApiRequestParams.SOURCE_CITY_NAME, RestApiRequestParams.DESTINATION_CITY_NAME },
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<FlightPlan> findShortestPathBetweenCities(
      @RequestParam(RestApiRequestParams.SOURCE_CITY_NAME) String sourceCityName,
      @RequestParam(RestApiRequestParams.DESTINATION_CITY_NAME) String destinationCityName) {
    FlightPlan flightPlan = cheapestPathFinder.findShortestPath(sourceCityName, destinationCityName);

    return ResponseEntity.ok(flightPlan);
  }

  @PreAuthorize("hasAnyAuthority('ROLE_USER', 'ROLE_ADMINISTRATOR')")
  @GetMapping(
      params = { RestApiRequestParams.SOURCE_CITY_NAME, RestApiRequestParams.DESTINATION_CITY_NAME,
          RestApiRequestParams.ROUTES_LIMIT },
      produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<List<FlightPlan>> findShortestPathsBetweenCities(
      @RequestParam(RestApiRequestParams.SOURCE_CITY_NAME) String sourceCityName,
      @RequestParam(RestApiRequestParams.DESTINATION_CITY_NAME) String destinationCityName,
      @RequestParam(RestApiRequestParams.ROUTES_LIMIT) int routesLimit) {
    BadRequestUtils.throwInvalidRequestDataIf(routesLimit < 1 || routesLimit > 5,
        RestApiErrors.INVALID_VALUE_ROUTES_LIMIT);

    List<FlightPlan> flightPlans = Lists.newArrayList();
    if (routesLimit == 1) {
      flightPlans.add(cheapestPathFinder.findShortestPath(sourceCityName, destinationCityName));
    } else if (routesLimit >= 1 && routesLimit <= 5) {
      flightPlans = cheapestPathFinder.findShortestPaths(sourceCityName, destinationCityName, routesLimit);
    }

    return ResponseEntity.ok(flightPlans);
  }
}
