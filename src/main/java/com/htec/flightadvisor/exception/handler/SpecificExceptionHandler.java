package com.htec.flightadvisor.exception.handler;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.api.ReturnCode;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.exception.ApiError;
import com.htec.flightadvisor.exception.BadRequestWithExplanationException;
import com.htec.flightadvisor.exception.InsufficientPrivileges;
import com.htec.flightadvisor.exception.NoPathException;
import com.htec.flightadvisor.exception.NotFoundWithExplanationException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SpecificExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpecificExceptionHandler.class);

  @ExceptionHandler(BadRequestWithExplanationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleBadRequestWithExplanation(BadRequestWithExplanationException e) {
    LOGGER.error("{}", e.getExplanation());

    return ApiError.fromBadRequestWithExplanation(e);
  }

  @ExceptionHandler(SQLException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleSQLException(SQLException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.fromIntegrityViolationException();
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleDataIntegrityViolationException(DataIntegrityViolationException e) {
    LOGGER.error("{}", e);

    return ApiError.fromIntegrityViolationException();
  }

  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
    LOGGER.error("{}", e);

    return ApiError.builder().code(ReturnCode.METHOD_NOT_SUPPORTED.getCode())
        .message(ReturnCode.METHOD_NOT_SUPPORTED.getMessage()).build();
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleInvalidJson(HttpMessageNotReadableException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.INVALID_JSON.getCode()).message(ReturnCode.INVALID_JSON.getMessage())
        .build();
  }

  @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.INVALID_MEDIA_TYPE.getCode())
        .message(ReturnCode.INVALID_MEDIA_TYPE.getMessage()).build();
  }

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleMediaTypeNotSupported(HttpMediaTypeNotSupportedException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.INVALID_MEDIA_TYPE.getCode())
        .message(ReturnCode.INVALID_MEDIA_TYPE.getMessage()).build();
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.INVALID_METHOD_ARGUMENT.getCode())
        .message(ReturnCode.INVALID_METHOD_ARGUMENT.getMessage()).build();
  }

  @ExceptionHandler(NotFoundWithExplanationException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ApiError handleNotFoundWithExplanationException(NotFoundWithExplanationException e) {
    LOGGER.error("{}", e.getExplanation());

    return ApiError.fromNotFoundWithExplanationException(e);
  }

  @ExceptionHandler(AccessDeniedException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public ApiError handleAccessDeniedException() {
    LOGGER.error("Unauthorized request.");

    return ApiError.fromAccessDeniedException();
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.MISSING_REQUEST_PARAM.getCode())
        .message(ReturnCode.MISSING_REQUEST_PARAM.getMessage()).build();
  }

  @ExceptionHandler(MultipartException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleMultipartException(MultipartException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.MULTIPART_REQUEST_REQUIRED.getCode())
        .message(ReturnCode.MULTIPART_REQUEST_REQUIRED.getMessage()).build();
  }

  @ExceptionHandler(InsufficientPrivileges.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public ApiError handleInsufficientPrivileges(InsufficientPrivileges e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.INSUFFICIENT_PRIVILEGES.getCode())
        .message(ReturnCode.INSUFFICIENT_PRIVILEGES.getMessage()).build();
  }

  @ExceptionHandler(NoSuchClientException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public ApiError handleNoSuchClientException(NoSuchClientException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.INVALID_CLIENT_ID.getCode())
        .message(ReturnCode.INVALID_CLIENT_ID.getMessage()).build();
  }

  @ExceptionHandler(NoPathException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public DynamicResponse handleNoPathException(NoPathException e) {
    LOGGER.error("No path found for '{}->{}'.", e.getSourceCityName(), e.getDestinationCityName());

    return new DynamicResponse(RestApiConstants.MESSAGE, RestApiErrors.NO_PATH_BETWEEN_CITIES);
  }

  @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ApiError handleUnsatisfiedServletRequestParameterException(UnsatisfiedServletRequestParameterException e) {
    LOGGER.error("{}", e.getMessage());

    return ApiError.builder().code(ReturnCode.MISSING_REQUEST_PARAM.getCode())
        .message(ReturnCode.MISSING_REQUEST_PARAM.getMessage()).build();
  }
}
