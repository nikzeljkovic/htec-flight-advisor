package com.htec.flightadvisor.exception;

import com.htec.flightadvisor.common.api.ReturnCode;

public class BadRequestWithExplanationException extends RuntimeException {

  private final ReturnCode returnCode;
  private final String explanation;

  public BadRequestWithExplanationException(ReturnCode returnCode, String explanation) {
    this.returnCode = returnCode;
    this.explanation = explanation;
  }

  public ReturnCode getReturnCode() {
    return returnCode;
  }

  public String getExplanation() {
    return explanation;
  }
}
