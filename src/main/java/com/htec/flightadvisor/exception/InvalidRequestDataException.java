package com.htec.flightadvisor.exception;

import com.htec.flightadvisor.common.api.ReturnCode;

public class InvalidRequestDataException extends BadRequestWithExplanationException {

  public InvalidRequestDataException(String explanation) {
    super(ReturnCode.INVALID_REQUEST_DATA, explanation);
  }
}
