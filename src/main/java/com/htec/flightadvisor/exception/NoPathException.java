package com.htec.flightadvisor.exception;

public class NoPathException extends RuntimeException {

  private final String sourceCityName;

  private final String destinationCityName;

  public NoPathException(String sourceCityName, String destinationCityName) {
    this.sourceCityName = sourceCityName;
    this.destinationCityName = destinationCityName;
  }

  public String getSourceCityName() {
    return sourceCityName;
  }

  public String getDestinationCityName() {
    return destinationCityName;
  }
}
