package com.htec.flightadvisor.validator.dto;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.utils.BadRequestUtils;
import com.htec.flightadvisor.dto.CommentDto;

@Service
public class CommentDtoValidator {

  public void validateCreateRequest(CommentDto commentDto) {
    BadRequestUtils.throwInvalidRequestDataIf(commentDto.getId() != null,
        RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));

    BadRequestUtils.throwInvalidRequestDataIf(commentDto.getDescription() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DESCRIPTION));
    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(commentDto.getDescription()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.DESCRIPTION));

    BadRequestUtils.throwInvalidRequestDataIf(commentDto.getCity() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY));
    BadRequestUtils.throwInvalidRequestDataIf(commentDto.getCity().getId() == null,
        RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY));
  }

  public void validateUpdateRequest(CommentDto commentDto) {
    BadRequestUtils.throwInvalidRequestDataIf(commentDto.getId() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));

    if (commentDto.getDescription() != null) {
      BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(commentDto.getDescription()),
          RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.DESCRIPTION));
    }
  }
}
