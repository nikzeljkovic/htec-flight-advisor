package com.htec.flightadvisor.validator.dto;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.utils.BadRequestUtils;
import com.htec.flightadvisor.dto.CityDto;

@Service
public class CityDtoValidator {

  public void validateCreateRequest(CityDto cityDto) {
    BadRequestUtils.throwInvalidRequestDataIf(cityDto.getId() != null,
        RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));

    BadRequestUtils.throwInvalidRequestDataIf(cityDto.getName() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(cityDto.getName()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));

    BadRequestUtils.throwInvalidRequestDataIf(cityDto.getCountry() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.COUNTRY));
    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(cityDto.getCountry()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.COUNTRY));

    BadRequestUtils.throwInvalidRequestDataIf(cityDto.getDescription() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DESCRIPTION));
    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(cityDto.getDescription()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.DESCRIPTION));

    BadRequestUtils.throwInvalidRequestDataIf(!CollectionUtils.isEmpty(cityDto.getComments()),
        RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.COMMENTS));
  }
}
