package com.htec.flightadvisor.validator.dto;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.utils.BadRequestUtils;
import com.htec.flightadvisor.dto.AccountDto;

@Service
public class AccountDtoValidator {

  public void validateRegistrationRequest(AccountDto accountDto) {
    BadRequestUtils.throwInvalidRequestDataIf(accountDto.getId() != null,
        RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));

    BadRequestUtils.throwInvalidRequestDataIf(accountDto.getEmail() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL));

    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getEmail()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL));

    BadRequestUtils.throwInvalidRequestDataIf(!EmailValidator.getInstance().isValid(accountDto.getEmail()),
        RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL));

    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getPassword()),
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD));

    BadRequestUtils.throwInvalidRequestDataIf(accountDto.getPassword().length() < 8,
        RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH));

    BadRequestUtils.throwInvalidRequestDataIf(accountDto.getFirstName() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME));

    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getFirstName()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));

    BadRequestUtils.throwInvalidRequestDataIf(accountDto.getLastName() == null,
        RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME));

    BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getLastName()),
        RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));

    BadRequestUtils.throwInvalidRequestDataIf(CollectionUtils.isNotEmpty(accountDto.getRoles()),
        RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES));
  }
}
