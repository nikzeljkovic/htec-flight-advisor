package com.htec.flightadvisor.validator.db;

import org.springframework.stereotype.Service;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.entity.Comment;
import com.htec.flightadvisor.exception.InsufficientPrivileges;
import com.htec.flightadvisor.exception.NotFoundWithExplanationException;
import com.htec.flightadvisor.repository.CommentsRepository;

@Service
public class CommentDbValidator {

  private final CommentsRepository commentsRepository;

  private final CityDbValidator cityDbValidator;

  public CommentDbValidator(CommentsRepository commentsRepository, CityDbValidator cityDbValidator) {
    this.commentsRepository = commentsRepository;
    this.cityDbValidator = cityDbValidator;
  }

  public void validateCreateRequest(CommentDto commentDto) {
    cityDbValidator.validateCityExistsById(commentDto.getCity().getId());
  }

  public Comment validateDeleteRequest(Long commentId, String requesterEmail) {
    Comment comment = validateExistsById(commentId);

    if (comment.getAccount().getEmail().equals(requesterEmail)) {
      return comment;
    } else {
      throw new InsufficientPrivileges();
    }
  }

  private Comment validateExistsById(Long commentId) {
    return commentsRepository.findById(commentId).orElseThrow(() -> new NotFoundWithExplanationException(
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.COMMENT, RestApiConstants.ID)));
  }

  public Comment validateUpdateRequest(CommentDto commentDto, String requesterEmail) {
    Comment comment = validateExistsById(commentDto.getId());

    if (comment.getAccount().getEmail().equals(requesterEmail)) {
      return comment;
    } else {
      throw new InsufficientPrivileges();
    }
  }
}
