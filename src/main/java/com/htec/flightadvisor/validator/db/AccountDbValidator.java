package com.htec.flightadvisor.validator.db;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.utils.BadRequestUtils;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.exception.NotFoundWithExplanationException;
import com.htec.flightadvisor.repository.AccountRepository;

@Service
public class AccountDbValidator {

  private final AccountRepository accountRepository;

  public AccountDbValidator(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  public Account validateFindByEmailRequest(String email) {
    return validateAccountExistsByEmail(email);
  }

  private Account validateAccountExistsByEmail(String email) {
    return accountRepository.findByEmail(email).orElseThrow(() -> new NotFoundWithExplanationException(
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)));
  }

  public void validateRegistrationRequest(AccountDto accountDto) {
    Optional<Account> account = accountRepository.findByEmail(accountDto.getEmail());
    BadRequestUtils.throwInvalidRequestDataIf(account.isPresent(),
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL));
  }
}
