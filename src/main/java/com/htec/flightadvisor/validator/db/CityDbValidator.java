package com.htec.flightadvisor.validator.db;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.api.RestApiErrors;
import com.htec.flightadvisor.common.utils.BadRequestUtils;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.exception.NotFoundWithExplanationException;
import com.htec.flightadvisor.repository.CityRepository;

@Service
public class CityDbValidator {

  private final CityRepository cityRepository;

  public CityDbValidator(CityRepository cityRepository) {
    this.cityRepository = cityRepository;
  }

  public City validateCityExistsByName(String name) {
    return cityRepository.findByName(name).orElseThrow(() -> new NotFoundWithExplanationException(
        RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY, RestApiConstants.NAME)));
  }

  public void validateCreateRequest(CityDto cityDto) {
    validateCityDoesNotExistByName(cityDto);
  }

  private void validateCityDoesNotExistByName(CityDto cityDto) {
    Optional<City> city = cityRepository.findByName(cityDto.getName());
    BadRequestUtils.throwInvalidRequestDataIf(city.isPresent(),
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.CITY, RestApiConstants.NAME));
  }

  public void validateCityExistsById(Long cityId) {
    Optional<City> city = cityRepository.findById(cityId);
    BadRequestUtils.throwInvalidRequestDataIf(!city.isPresent(),
        RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.CITY, RestApiConstants.ID));
  }
}
