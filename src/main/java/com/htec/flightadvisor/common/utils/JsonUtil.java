package com.htec.flightadvisor.common.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.entity.Comment;

@Service
public class JsonUtil {

  private final ModelMapper modelMapper;

  public JsonUtil(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public AccountDto map(Account account) {
    hideUnimportantFields(account);
    return modelMapper.map(account, AccountDto.class);
  }

  public CityDto map(City city, Integer commentsLimit) {
    hideUnimportantFields(city);
    hideUnimportantFields(city.getComments());

    if (commentsLimit != null) {
      limitCommentNumber(city, commentsLimit);
    }

    return modelMapper.map(city, CityDto.class);
  }

  private void hideUnimportantFields(List<Comment> comments) {
    comments.forEach(this::hideUnimportantFields);
  }

  private void limitCommentNumber(City city, Integer commentsLimit) {
    List<Comment> comments = city.getComments().stream().limit(commentsLimit).collect(Collectors.toList());
    city.setComments(comments);
  }

  private void hideUnimportantFields(Comment comment) {
    comment.setCity(null);
    hideUnimportantFields(comment.getAccount());
  }

  private void hideUnimportantFields(City city) {
    if (city == null) {
      return;
    }
    city.setAirports(null);
  }

  private static void hideUnimportantFields(Account account) {
    if (account == null) {
      return;
    }
    account.setPassword(null);
  }
}
