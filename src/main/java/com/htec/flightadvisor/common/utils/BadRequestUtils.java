package com.htec.flightadvisor.common.utils;

import com.htec.flightadvisor.exception.InvalidRequestDataException;

public final class BadRequestUtils {

  public static void throwInvalidRequestDataIf(boolean condition, String explanation) {
    if (condition) {
      throwInvalidRequestData(explanation);
    }
  }

  private static void throwInvalidRequestData(String explanation) {
    throw new InvalidRequestDataException(explanation);
  }

  private BadRequestUtils() {
  }
}
