package com.htec.flightadvisor.common.api;

public final class RestApiConstants {

  public static final String ACCOUNT = "account";
  public static final String CITY = "city";
  public static final String CODE = "code";
  public static final String COMMENT = "comment";
  public static final String COMMENTS = "comments";
  public static final String COST = "cost";
  public static final String COUNTRY = "country";
  public static final String CREATED_AT = "createdAt";
  public static final String DATA = "data";
  public static final String DESCRIPTION = "description";
  public static final String DESTINATION_AIRPORT_ID = "destinationAirportId";
  public static final String DESTINATION_AIRPORT_NAME = "destinationAirportName";
  public static final String DESTINATION_CITY_NAME = "destinationCityName";
  public static final String EMAIL = "email";
  public static final String EXPLANATION = "explanation";
  public static final String FIRST_NAME = "firstName";
  public static final String FLIGHTS = "flights";
  public static final String ID = "id";
  public static final String IS_DEFAULT = "isDefault";
  public static final String LABEL = "label";
  public static final String LAST_NAME = "lastName";
  public static final String LENGTH = "length";
  public static final String MESSAGE = "message";
  public static final String NAME = "name";
  public static final String PASSWORD = "password";
  public static final String ROLES = "roles";
  public static final String ROUTE_ID = "routeId";
  public static final String SOURCE_AIRPORT_ID = "sourceAirportId";
  public static final String SOURCE_AIRPORT_NAME = "sourceAirportName";
  public static final String SOURCE_CITY_NAME = "sourceCityName";
  public static final String UPDATED_AT = "updatedAt";
  public static final int PASSWORD_MIN_LENGTH = 8;

  private RestApiConstants() {
  }
}
