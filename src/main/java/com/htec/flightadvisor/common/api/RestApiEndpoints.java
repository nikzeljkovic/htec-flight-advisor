package com.htec.flightadvisor.common.api;

public final class RestApiEndpoints {

  private static final String API_ROOT = "/api/v1";

  public static final String ACCOUNT = API_ROOT + "/account";
  public static final String ACCOUNTS = API_ROOT + "/accounts";

  public static final String CITY = API_ROOT + "/city";
  public static final String CITIES = API_ROOT + "/cities";

  public static final String COMMENT = API_ROOT + "/comment";
  public static final String COMMENTS = API_ROOT + "/comments";

  public static final String ROUTES = API_ROOT + "/routes";

  private RestApiEndpoints() {
  }
}
