package com.htec.flightadvisor.common.api;

public final class RestApiErrors {

  public static final String INVALID_VALUE_ROUTES_LIMIT =
      "Invalid value for request param '" + RestApiRequestParams.ROUTES_LIMIT + "'. Value should be between 1 and 5.";

  public static final String NO_PATH_BETWEEN_CITIES = "There is no path between two given cities.";

  public static String fieldShouldBeNull(String fieldName) {
    return String.format("Field `%s` should be null.", fieldName);
  }

  public static String fieldShouldNotBeNull(String fieldName) {
    return String.format("Field `%s` should not be null.", fieldName);
  }

  public static String fieldShouldNotBeEmptyString(String fieldName) {
    return String.format("Field `%s` should not be empty string.", fieldName);
  }

  public static String fieldLengthShouldBeAtLeast(String fieldName, int length) {
    return String.format("Field `%s` should be at least %d characters long.", fieldName, length);
  }

  public static String invalidFieldFormat(String fieldName) {
    return String.format("Invalid format provided for field `%s`.", fieldName);
  }

  public static String entityWithGivenFieldAlreadyExists(String entityName, String fieldName) {
    return String.format("%s with given `%s` already exists.", entityName, fieldName);
  }

  public static String entityWithGivenFieldDoesNotExist(String entityName, String fieldName) {
    return String.format("%s with given `%s` does not exist.", entityName, fieldName);
  }

  public static String fieldInsideFieldShouldNotBeNull(String fieldName, String insideFieldName) {
    return String.format("Field `%s` inside field `%s` should not be null.", fieldName, insideFieldName);
  }

  public static String fieldShouldBeNullOrEmptyCollection(String fieldName) {
    return String.format("Field `%s` should be null or empty collection.", fieldName);
  }

  private RestApiErrors() {
  }
}
