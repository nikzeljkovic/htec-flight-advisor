package com.htec.flightadvisor.common.api;

public final class RestApiRequestParams {

  public static final String COMMENTS_LIMIT = "comments-limit";
  public static final String DESTINATION_CITY_NAME = "destination-city-name";
  public static final String NAME = "name";
  public static final String SOURCE_CITY_NAME = "source-city-name";
  public static final String ROUTES_LIMIT = "routes-limit";

  private RestApiRequestParams() {
  }
}
