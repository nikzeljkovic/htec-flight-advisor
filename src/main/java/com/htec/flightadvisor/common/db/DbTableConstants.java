package com.htec.flightadvisor.common.db;

public final class DbTableConstants {

  public static final String ACCOUNT_ROLE_TABLE = "account_role";
  public static final String ACCOUNT_TABLE = "account";
  public static final String AIRPORT_TABLE = "airport";
  public static final String CITY_TABLE = "city";
  public static final String COMMENT_TABLE = "comment";
  public static final String ROLE_TABLE = "role";
  public static final String ROUTE_TABLE = "route";

  private DbTableConstants() {
  }
}
