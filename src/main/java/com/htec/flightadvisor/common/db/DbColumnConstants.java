package com.htec.flightadvisor.common.db;

public final class DbColumnConstants {

  public static final String ACCOUNT_ID = "id_account";
  public static final String AIRLINE = "airline";
  public static final String AIRLINE_ID = "id_airline";
  public static final String AIRPORT_ID = "id_airport";
  public static final String AIRPORT_NAME = "airport_name";
  public static final String ALTITUDE = "altitude";
  public static final String CITY_ID = "id_city";
  public static final String CITY_NAME = "city_name";
  public static final String CODESHARE = "codeshare";
  public static final String COMMENT_ID = "id_comment";
  public static final String COUNTRY = "country";
  public static final String CREATED_AT = "created_at";
  public static final String DESCRIPTION = "description";
  public static final String DESTINATION_AIRPORT = "destination_airport";
  public static final String DESTINATION_AIRPORT_ID = "id_destination_airport";
  public static final String DST = "dst";
  public static final String EMAIL = "email";
  public static final String EQUIPMENT = "equipment";
  public static final String FIRST_NAME = "first_name";
  public static final String FK_ACCOUNT_ID = "fk_id_account";
  public static final String FK_CITY_ID = "fk_id_city";
  public static final String IATA = "iata";
  public static final String ICAO = "icao";
  public static final String IS_DEFAULT = "is_default";
  public static final String LABEL = "label";
  public static final String LAST_NAME = "last_name";
  public static final String LATITUDE = "latitude";
  public static final String LONGITUDE = "longitude";
  public static final String OLSON = "olson";
  public static final String PASSWORD = "password";
  public static final String PRICE = "price";
  public static final String ROLE_ID = "id_role";
  public static final String ROUTE_ID = "id_route";
  public static final String SOURCE = "source";
  public static final String SOURCE_AIRPORT = "source_airport";
  public static final String SOURCE_AIRPORT_ID = "id_source_airport";
  public static final String STOPS = "stops";
  public static final String TIMEZONE = "timezone";
  public static final String TYPE = "type";
  public static final String UPDATED_AT = "updated_at";

  private DbColumnConstants() {
  }
}
