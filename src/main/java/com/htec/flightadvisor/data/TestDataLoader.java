package com.htec.flightadvisor.data;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.htec.flightadvisor.repository.AirportRepository;
import com.htec.flightadvisor.repository.CityRepository;
import com.htec.flightadvisor.repository.RouteRepository;

@Component
@Profile({ "test" })
public class TestDataLoader extends DataLoader {

  private static final Logger LOGGER = LoggerFactory.getLogger(TestDataLoader.class);

  @Autowired
  private AirportRepository airportRepository;

  @Autowired
  private RouteRepository routeRepository;

  @Autowired
  private CityRepository cityRepository;

  @Value("${flight-data.test-airports}")
  private Resource airports;

  @Value("${flight-data.test-routes}")
  private Resource routes;

  @PostConstruct
  public void init() {
    LOGGER.info("Loading test flights data.");
    super.loadDataFromFileAndStoreInDatabase(airports, airportRepository, routes, routeRepository, cityRepository);
    LOGGER.info("Test data flights loaded.");
  }
}
