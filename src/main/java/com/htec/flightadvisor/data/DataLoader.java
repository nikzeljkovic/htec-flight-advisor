package com.htec.flightadvisor.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

import com.htec.flightadvisor.algorithm.GeographicalDistance;
import com.htec.flightadvisor.algorithm.Heuristics;
import com.htec.flightadvisor.entity.Airport;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.entity.Route;
import com.htec.flightadvisor.repository.AirportRepository;
import com.htec.flightadvisor.repository.CityRepository;
import com.htec.flightadvisor.repository.RouteRepository;
import com.opencsv.CSVReader;

import io.jsonwebtoken.lang.Collections;

public class DataLoader {

  private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);

  private static final int AIRPORT_FILE_ENTRY_FIELDS = 14;

  private static final int ROUTE_FILE_ENTRY_FIELDS = 10;

  private AirportRepository airportRepository;

  private RouteRepository routeRepository;

  private CityRepository cityRepository;

  public void loadDataFromFileAndStoreInDatabase(Resource airports, AirportRepository airportRepository,
      Resource routes, RouteRepository routeRepository, CityRepository cityRepository) {
    this.airportRepository = airportRepository;
    this.routeRepository = routeRepository;
    this.cityRepository = cityRepository;

    loadAndStoreAirports(airports);
    loadAndStoreRoutes(routes);
    calculateHeuristicMultiplier();

    System.out.println("City count: " + cityRepository.count());
  }

  private void calculateHeuristicMultiplier() {
    LOGGER.info("Started heuristic multiplier calculation.");

    /*
     * Version 1
     */
    Double multiplier = Double.MAX_VALUE;
    for (Airport airport : airportRepository.findAll()) {
      for (Route route : routeRepository.findBySourceAirportId(airport.getId())) {
        Double tempMultiplier = route.getPrice() / GeographicalDistance.getEuclideanDistance(airport,
            airportRepository.findById(route.getDestinationAirportId()).orElse(null));

        multiplier = Math.min(multiplier, tempMultiplier);
      }
    }
    Heuristics.multiplier = multiplier;

    /*
     * Version 2
     */
    // Double maxDistance = Double.MIN_VALUE;
    // Double minPrice = Double.MAX_VALUE;
    //
    // for (Airport airport : airportRepository.findAll()) {
    // for (Route route : routeRepository.findBySourceAirportId(airport.getId())) {
    // maxDistance = Math.max(maxDistance, GeographicalDistance.getEuclideanDistance(airport,
    // airportRepository.findById(route.getDestinationAirportId()).orElse(null)));
    // minPrice = Math.min(minPrice, route.getPrice());
    // }
    // }
    //
    // Heuristics.multiplier = minPrice / maxDistance;
    LOGGER.info("Finished heuristic multiplier calculation.");
  }

  private void loadAndStoreAirports(Resource airports) {
    LOGGER.info("Started loading airports from file '{}'.", airports.getFilename());

    try {
      Reader reader = new BufferedReader(new InputStreamReader(airports.getInputStream()));
      try (CSVReader csvReader = new CSVReader(reader)) {
        String[] values;
        while ((values = csvReader.readNext()) != null) {
          Airport airport = getAirportFromFileEntry(Arrays.asList(values));

          if (airport != null) {
            airportRepository.save(airport);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      LOGGER.error("Error while processing airports from file '{}'. Exception: {}", airports.getFilename(),
          e.getMessage());
    }

    LOGGER.info("Finished loading airports from file '{}'.", airports.getFilename());
  }

  private Airport getAirportFromFileEntry(List<String> fields) {
    if (Collections.size(fields) == AIRPORT_FILE_ENTRY_FIELDS) {
      return buildAirportFromFileEntry(fields);
    } else {
      LOGGER.error("Invalid airport file entry '{}'.", fields);
    }
    return null;
  }

  private Airport buildAirportFromFileEntry(List<String> fields) {
    String cityName = fields.get(2);
    String country = fields.get(3);
    City city = cityRepository.findByName(cityName).orElse(null);

    if (city == null) {
      city = addCity(cityName, country);
    }

    return Airport.builder().id(Long.valueOf(fields.get(0))).name(fields.get(1)).city(city).country(country)
        .iata(fields.get(4)).icao(fields.get(5)).latitude(Double.valueOf(fields.get(6)))
        .longitude(Double.valueOf(fields.get(7))).altitude(Integer.valueOf(fields.get(8))).timezone(fields.get(9))
        .dst(fields.get(10)).olson(fields.get(11)).type(fields.get(12)).source(fields.get(13)).build();
  }

  private City addCity(String cityName, String country) {
    City city = City.builder().name(cityName).description("Dummy city description.").country(country).build();
    city = cityRepository.save(city);
    return city;
  }

  private void loadAndStoreRoutes(Resource routes) {
    LOGGER.info("Started loading routes from file '{}'.", routes.getFilename());

    try {
      Reader reader = new BufferedReader(new InputStreamReader(routes.getInputStream()));
      try (CSVReader csvReader = new CSVReader(reader)) {
        String[] values;
        while ((values = csvReader.readNext()) != null) {
          Route route = getRouteFromFileEntry(Arrays.asList(values));

          if (route != null) {
            routeRepository.save(route);
          }
        }
      }
    } catch (Exception e) {
      LOGGER.error("Error while processing routes from file '{}'. Exception: {}", routes.getFilename(), e.getMessage());
    }

    LOGGER.info("Finished loading routes from file '{}'.", routes.getFilename());
  }

  private Route getRouteFromFileEntry(List<String> fields) {
    if (Collections.size(fields) == ROUTE_FILE_ENTRY_FIELDS) {
      return buildRouteFromFileEntry(fields);
    } else {
      LOGGER.error("Invalid route file entry '{}'.", fields);
    }
    return null;
  }

  private Route buildRouteFromFileEntry(List<String> fields) {
    Long airlineId = getIdFromString(fields.get(1));
    Long sourceAirportId = getIdFromString(fields.get(3));
    Long destinationAirportId = getIdFromString(fields.get(5));

    if (airlineId == -1 || sourceAirportId == -1 || destinationAirportId == -1) {
      return null;
    } else {
      List<Long> airportIds = Arrays.asList(sourceAirportId, destinationAirportId);
      List<Airport> airportsInDb = airportRepository.findAllById(airportIds);

      if (airportsInDb.size() != airportIds.size()) {
        return null;
      }
    }

    return Route.builder().airline(fields.get(0)).airlineId(airlineId).sourceAirport(fields.get(2))
        .sourceAirportId(sourceAirportId).destinationAirport(fields.get(4)).destinationAirportId(destinationAirportId)
        .codeshare(fields.get(6)).stops(Integer.valueOf(fields.get(7))).equipment(fields.get(8))
        .price(Double.valueOf(fields.get(9))).build();
  }

  private Long getIdFromString(String id) {
    return id.equals("N") ? -1 : Long.valueOf(id);
  }
}
