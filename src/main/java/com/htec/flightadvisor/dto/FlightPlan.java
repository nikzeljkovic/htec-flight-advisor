package com.htec.flightadvisor.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.htec.flightadvisor.common.api.RestApiConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlightPlan {

  @JsonProperty(RestApiConstants.SOURCE_AIRPORT_ID)
  private Long sourceAirportId;

  @JsonProperty(RestApiConstants.SOURCE_AIRPORT_NAME)
  private String sourceAirportName;

  @JsonProperty(RestApiConstants.SOURCE_CITY_NAME)
  private String sourceCityName;

  @JsonProperty(RestApiConstants.DESTINATION_AIRPORT_ID)
  private Long destinationAirportId;

  @JsonProperty(RestApiConstants.DESTINATION_AIRPORT_NAME)
  private String destinationAirportName;

  @JsonProperty(RestApiConstants.DESTINATION_CITY_NAME)
  private String destinationCityName;

  @JsonProperty(RestApiConstants.LENGTH)
  private Double length;

  @JsonProperty(RestApiConstants.COST)
  private Double cost;

  @JsonProperty(RestApiConstants.FLIGHTS)
  private List<Flight> flights;
}
