package com.htec.flightadvisor.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.htec.flightadvisor.common.api.RestApiConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CityDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.NAME)
  private String name;

  @JsonProperty(RestApiConstants.COUNTRY)
  private String country;

  @JsonProperty(RestApiConstants.DESCRIPTION)
  private String description;

  @JsonProperty(RestApiConstants.COMMENTS)
  private List<CommentDto> comments;
}
