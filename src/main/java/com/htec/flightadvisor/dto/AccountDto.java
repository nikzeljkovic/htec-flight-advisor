package com.htec.flightadvisor.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.htec.flightadvisor.common.api.RestApiConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.EMAIL)
  private String email;

  @JsonProperty(RestApiConstants.PASSWORD)
  private String password;

  @JsonProperty(RestApiConstants.FIRST_NAME)
  private String firstName;

  @JsonProperty(RestApiConstants.LAST_NAME)
  private String lastName;

  @JsonProperty(RestApiConstants.ROLES)
  private List<RoleDto> roles = new ArrayList<>();
}
