package com.htec.flightadvisor.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.entity.Airport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Flight {

  @JsonProperty(RestApiConstants.ROUTE_ID)
  private Long routeId;

  @JsonProperty(RestApiConstants.SOURCE_AIRPORT_ID)
  private Long sourceAirportId;

  @JsonProperty(RestApiConstants.SOURCE_AIRPORT_NAME)
  private String sourceAirportName;

  @JsonProperty(RestApiConstants.SOURCE_CITY_NAME)
  private String sourceCityName;

  @JsonProperty(RestApiConstants.DESTINATION_AIRPORT_ID)
  private Long destinationAirportId;

  @JsonProperty(RestApiConstants.DESTINATION_AIRPORT_NAME)
  private String destinationAirportName;

  @JsonProperty(RestApiConstants.DESTINATION_CITY_NAME)
  private String destinationCityName;

  @JsonProperty(RestApiConstants.LENGTH)
  private Double length;

  @JsonProperty(RestApiConstants.COST)
  private Double cost;

  public Flight(Airport source, Airport destination, Double flightCost, Double flightLength, Long routeId) {
    this.sourceAirportId = source.getId();
    this.sourceAirportName = source.getName();
    this.sourceCityName = source.getCity().getName();
    this.destinationAirportId = destination.getId();
    this.destinationAirportName = destination.getName();
    this.destinationCityName = destination.getCity().getName();
    this.length = flightLength;
    this.cost = flightCost;
    this.routeId = routeId;
  }
}
