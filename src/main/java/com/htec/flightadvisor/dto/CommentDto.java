package com.htec.flightadvisor.dto;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.utils.JsonJodaDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.DESCRIPTION)
  private String description;

  @JsonProperty(RestApiConstants.CREATED_AT)
  @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
  private DateTime createdAt;

  @JsonProperty(RestApiConstants.UPDATED_AT)
  @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
  private DateTime updatedAt;

  @JsonProperty(RestApiConstants.CITY)
  private CityDto city;

  @JsonProperty(RestApiConstants.ACCOUNT)
  private AccountDto account;
}
