package com.htec.flightadvisor.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.htec.flightadvisor.common.api.RestApiConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.LABEL)
  private String label;

  @JsonProperty(RestApiConstants.IS_DEFAULT)
  private boolean isDefault;
}
