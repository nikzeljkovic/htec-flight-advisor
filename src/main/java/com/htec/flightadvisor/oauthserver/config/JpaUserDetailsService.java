package com.htec.flightadvisor.oauthserver.config;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.entity.Role;
import com.htec.flightadvisor.repository.AccountRepository;

@Service
public class JpaUserDetailsService implements UserDetailsService {

  private final AccountRepository accountRepository;

  @Autowired
  public JpaUserDetailsService(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String email) {
    Optional<Account> accountOptional = accountRepository.findByEmail(email);

    if (!accountOptional.isPresent()) {
      throw new UsernameNotFoundException("Username not found.");
    }

    List<GrantedAuthority> grantedAuthorities = accountOptional.get().getRoles().stream().map(Role::getLabel)
        .map(SimpleGrantedAuthority::new).collect(Collectors.toList());

    return new User(accountOptional.get().getEmail(), accountOptional.get().getPassword(), true, true, true, true,
        grantedAuthorities);
  }
}
