package com.htec.flightadvisor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.entity.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

  List<Route> findBySourceAirportId(Long currentAirport);

  Route findFirstBySourceAirportIdAndDestinationAirportIdOrderByPriceAsc(Long currentAirportId,
      Long neighbourAirportId);

  List<Route> findBySourceAirportIdAndDestinationAirportId(Long sourceAirportId, Long destinationAirportId);
}
