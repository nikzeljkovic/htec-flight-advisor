package com.htec.flightadvisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.entity.Comment;

@Repository
public interface CommentsRepository extends JpaRepository<Comment, Long> {
}
