package com.htec.flightadvisor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.entity.Airport;

@Repository
public interface AirportRepository extends JpaRepository<Airport, Long> {

  List<Airport> findByCityId(Long id);
}
