package com.htec.flightadvisor.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htec.flightadvisor.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {

  Optional<City> findByName(String cityName);

  Page<City> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
