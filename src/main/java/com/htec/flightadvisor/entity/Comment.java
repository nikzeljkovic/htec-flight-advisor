package com.htec.flightadvisor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.htec.flightadvisor.common.db.DbColumnConstants;
import com.htec.flightadvisor.common.db.DbTableConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = DbTableConstants.COMMENT_TABLE)
public class Comment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = DbColumnConstants.COMMENT_ID)
  private Long id;

  @Column(name = DbColumnConstants.DESCRIPTION)
  private String description;

  @Column(name = DbColumnConstants.CREATED_AT)
  @Type(
      type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
      parameters = { @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
          @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC") })
  private DateTime createdAt;

  @Column(name = DbColumnConstants.UPDATED_AT)
  @Type(
      type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime",
      parameters = { @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
          @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC") })
  private DateTime updatedAt;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = DbColumnConstants.FK_CITY_ID)
  private City city;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = DbColumnConstants.FK_ACCOUNT_ID)
  private Account account;
}
