package com.htec.flightadvisor.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.htec.flightadvisor.common.db.DbColumnConstants;
import com.htec.flightadvisor.common.db.DbTableConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = DbTableConstants.CITY_TABLE)
public class City {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = DbColumnConstants.CITY_ID)
  private Long id;

  @Column(name = DbColumnConstants.CITY_NAME)
  private String name;

  @Column(name = DbColumnConstants.COUNTRY)
  private String country;

  @Column(name = DbColumnConstants.DESCRIPTION)
  private String description;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
  private List<Airport> airports;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
  private List<Comment> comments;
}
