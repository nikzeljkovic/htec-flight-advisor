package com.htec.flightadvisor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.htec.flightadvisor.common.db.DbColumnConstants;
import com.htec.flightadvisor.common.db.DbTableConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(
    name = DbTableConstants.ROUTE_TABLE,
    indexes = {
        @Index(
            name = "ix_route_table_id_source_airport_id_destination_airport",
            columnList = "id_source_airport,id_destination_airport"),
        @Index(name = "ix_route_table_id_source_airport", columnList = "id_source_airport") })
public class Route {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = DbColumnConstants.ROUTE_ID)
  private Long id;

  @Column(name = DbColumnConstants.AIRLINE)
  private String airline;

  @Column(name = DbColumnConstants.AIRLINE_ID)
  private Long airlineId;

  @Column(name = DbColumnConstants.SOURCE_AIRPORT)
  private String sourceAirport;

  @Column(name = DbColumnConstants.SOURCE_AIRPORT_ID)
  private Long sourceAirportId;

  @Column(name = DbColumnConstants.DESTINATION_AIRPORT)
  private String destinationAirport;

  @Column(name = DbColumnConstants.DESTINATION_AIRPORT_ID)
  private Long destinationAirportId;

  @Column(name = DbColumnConstants.CODESHARE)
  private String codeshare;

  @Column(name = DbColumnConstants.STOPS)
  private Integer stops;

  @Column(name = DbColumnConstants.EQUIPMENT)
  private String equipment;

  @Column(name = DbColumnConstants.PRICE)
  private Double price;
}
