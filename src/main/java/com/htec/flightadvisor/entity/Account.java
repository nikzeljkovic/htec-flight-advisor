package com.htec.flightadvisor.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.htec.flightadvisor.common.db.DbColumnConstants;
import com.htec.flightadvisor.common.db.DbTableConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.ACCOUNT_TABLE)
public class Account {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = DbColumnConstants.ACCOUNT_ID)
  private Long id;

  @Column(unique = true, name = DbColumnConstants.EMAIL)
  private String email;

  @Column(name = DbColumnConstants.PASSWORD)
  private String password;

  @Column(name = DbColumnConstants.FIRST_NAME)
  private String firstName;

  @Column(name = DbColumnConstants.LAST_NAME)
  private String lastName;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = DbTableConstants.ACCOUNT_ROLE_TABLE,
      joinColumns = @JoinColumn(name = DbColumnConstants.ACCOUNT_ID),
      inverseJoinColumns = @JoinColumn(name = DbColumnConstants.ROLE_ID))
  private List<Role> roles = new ArrayList<>();
}
