package com.htec.flightadvisor.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.htec.flightadvisor.common.db.DbColumnConstants;
import com.htec.flightadvisor.common.db.DbTableConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = DbTableConstants.AIRPORT_TABLE)
public class Airport {

  @Id
  @Column(name = DbColumnConstants.AIRPORT_ID)
  private Long id;

  @Column(name = DbColumnConstants.AIRPORT_NAME)
  private String name;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_CITY_ID)
  private City city;

  @Column(name = DbColumnConstants.COUNTRY)
  private String country;

  @Column(name = DbColumnConstants.IATA)
  private String iata;

  @Column(name = DbColumnConstants.ICAO)
  private String icao;

  @Column(name = DbColumnConstants.LONGITUDE)
  private Double longitude;

  @Column(name = DbColumnConstants.LATITUDE)
  private Double latitude;

  @Column(name = DbColumnConstants.ALTITUDE)
  private Integer altitude;

  @Column(name = DbColumnConstants.TIMEZONE)
  private String timezone;

  @Column(name = DbColumnConstants.DST)
  private String dst;

  @Column(name = DbColumnConstants.OLSON)
  private String olson;

  @Column(name = DbColumnConstants.TYPE)
  private String type;

  @Column(name = DbColumnConstants.SOURCE)
  private String source;

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Airport airport = (Airport) o;
    return Objects.equals(id, airport.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, city, country, iata, icao, longitude, latitude, altitude, timezone, dst, olson, type,
        source);
  }
}
