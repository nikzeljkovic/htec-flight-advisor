package com.htec.flightadvisor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.htec.flightadvisor.common.db.DbColumnConstants;
import com.htec.flightadvisor.common.db.DbTableConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = DbTableConstants.ROLE_TABLE)
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = DbColumnConstants.ROLE_ID)
  private Long id;

  @Column(name = DbColumnConstants.LABEL)
  private String label;

  @Column(name = DbColumnConstants.IS_DEFAULT)
  private boolean isDefault;
}
