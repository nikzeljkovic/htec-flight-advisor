package com.htec.flightadvisor.algorithm;

import com.htec.flightadvisor.entity.Airport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * This class represents node of the priority queue used when finding one cheapest route. It holds information which
 * airport it is and cost to it.
 */
public class AStarNode {

  private Airport airport;

  private Double cost;
}