package com.htec.flightadvisor.algorithm;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.assertj.core.util.Lists;
import org.springframework.stereotype.Service;

import com.htec.flightadvisor.dto.Flight;
import com.htec.flightadvisor.dto.FlightPlan;
import com.htec.flightadvisor.entity.Airport;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.entity.Route;
import com.htec.flightadvisor.repository.AirportRepository;
import com.htec.flightadvisor.repository.RouteRepository;

@Service
public class FlightPlanResponseBuilder {

  private final RouteRepository routeRepository;

  private final AirportRepository airportRepository;

  public FlightPlanResponseBuilder(RouteRepository routeRepository, AirportRepository airportRepository) {
    this.routeRepository = routeRepository;
    this.airportRepository = airportRepository;
  }

  /**
   * @param cameFrom
   *          - For each airport in map, we store from which airport we have come to it
   * @param source
   *          - Source city
   * @param destination
   *          - Airport in the destination city
   * @return FlightPlan object which has total cost and length of recommended route, and all paths of the route.
   */
  public FlightPlan buildFlightPlan(Map<Airport, Airport> cameFrom, City source, Airport destination) {
    FlightPlan flightPlan = FlightPlan.builder().sourceAirportName(source.getName()).sourceAirportId(source.getId())
        .sourceCityName(source.getName()).destinationAirportName(destination.getName())
        .destinationAirportId(destination.getId()).destinationCityName(destination.getCity().getName())
        .flights(Lists.newArrayList()).build();

    List<Airport> cheapestPaths = reconstructShortestPath(cameFrom, destination);

    return addShortestPathToFlightPlan(flightPlan, cheapestPaths);
  }

  private List<Airport> reconstructShortestPath(Map<Airport, Airport> cameFrom, Airport destination) {
    Airport current = destination;
    List<Airport> path = Lists.newArrayList();

    while (current != null) {
      path.add(current);
      current = cameFrom.get(current);
    }
    Collections.reverse(path);
    return path;
  }

  private FlightPlan addShortestPathToFlightPlan(FlightPlan flightPlan, List<Airport> path) {
    Double totalCost = 0D;
    Double totalLength = 0D;

    for (int i = 0; i < path.size() - 1; i++) {
      Airport source = path.get(i);
      Airport destination = path.get(i + 1);
      Route route = findShortestDirectRoute(source.getId(), destination.getId());

      Double flightCost = route.getPrice();
      Double flightLength = GeographicalDistance.getEuclideanDistance(source, destination);

      flightPlan.getFlights().add(new Flight(source, destination, flightCost, flightLength, route.getId()));
      totalCost += flightCost;
      totalLength += flightLength;
    }

    flightPlan.setCost(totalCost);
    flightPlan.setLength(totalLength);

    return flightPlan;
  }

  private Route findShortestDirectRoute(Long sourceAirportId, Long destinationAirportId) {
    return routeRepository.findFirstBySourceAirportIdAndDestinationAirportIdOrderByPriceAsc(sourceAirportId,
        destinationAirportId);
  }

  /**
   * @param currentNode
   *          - Node of the priority queue which have information which airport it is and path representing route to it
   * @param source
   *          - Source city
   * @param destination
   *          - Airport in the destination city
   * @return FlightPlan object which has total cost and length for each recommended route, and all paths of each route.
   */
  public FlightPlan buildFlightPlan(AStarNodeWithPath currentNode, City source, Airport destination) {
    FlightPlan flightPlan = FlightPlan.builder().sourceAirportName(source.getName()).sourceAirportId(source.getId())
        .sourceCityName(source.getName()).destinationAirportName(destination.getName())
        .destinationAirportId(destination.getId()).destinationCityName(destination.getCity().getName())
        .flights(Lists.newArrayList()).build();

    List<Long> routeIdPath = Lists.newArrayList(currentNode.getRouteIdsPath());

    return addShortestPathsToFlightPlan(flightPlan, routeIdPath);
  }

  private FlightPlan addShortestPathsToFlightPlan(FlightPlan flightPlan, List<Long> routeIdPath) {
    Double totalCost = 0D;
    Double totalLength = 0D;

    for (Long routeId : routeIdPath) {
      Route route = routeRepository.findById(routeId).orElse(null);
      Airport source = airportRepository.findById(route.getSourceAirportId()).orElse(null);
      Airport destination = airportRepository.findById(route.getDestinationAirportId()).orElse(null);

      Double flightCost = route.getPrice();
      Double flightLength = GeographicalDistance.getEuclideanDistance(source, destination);

      flightPlan.getFlights().add(new Flight(source, destination, flightCost, flightLength, route.getId()));
      totalCost += flightCost;
      totalLength += flightLength;
    }

    flightPlan.setCost(totalCost);
    flightPlan.setLength(totalLength);

    return flightPlan;
  }
}
