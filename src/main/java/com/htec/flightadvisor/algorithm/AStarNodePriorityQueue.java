package com.htec.flightadvisor.algorithm;

import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;

import com.google.common.collect.Maps;
import com.htec.flightadvisor.entity.Airport;

/**
 * Priority queue used when finding one cheapest path between two cities. Overriding it we are able to do some memory
 * optimization to the algorithm. We will store each node (airport) only once in the queue, and if we want to add it
 * again, we will check if we have it already on queue and update cost if we found lower one.
 */
public class AStarNodePriorityQueue extends PriorityQueue<AStarNode> {

  private Map<Long, AStarNode> airportIdToAStarNodeIndex = Maps.newHashMap();

  public AStarNodePriorityQueue(Comparator<? super AStarNode> comparator) {
    super(comparator);
  }

  @Override
  public boolean add(AStarNode aStarNode) {
    airportIdToAStarNodeIndex.put(aStarNode.getAirport().getId(), aStarNode);
    return super.add(aStarNode);
  }

  @Override
  public AStarNode poll() {
    AStarNode aStarNode = super.poll();

    if (aStarNode != null) {
      airportIdToAStarNodeIndex.remove(aStarNode.getAirport().getId());
    }
    return aStarNode;
  }

  private boolean containsAirport(Long airportId) {
    return airportIdToAStarNodeIndex.get(airportId) != null;
  }

  public boolean notContainsAirport(Long airportId) {
    return !containsAirport(airportId);
  }

  public Double getCost(Airport airport) {
    return airportIdToAStarNodeIndex.get(airport.getId()).getCost();
  }

  public void setCost(Airport airport, Double cost) {
    AStarNode aStarNode = airportIdToAStarNodeIndex.get(airport.getId());

    remove(aStarNode);
    aStarNode.setCost(cost);
    add(aStarNode);
  }
}
