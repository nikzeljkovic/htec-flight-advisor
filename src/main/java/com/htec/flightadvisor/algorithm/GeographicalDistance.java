package com.htec.flightadvisor.algorithm;

import com.htec.flightadvisor.entity.Airport;

public class GeographicalDistance {

  /**
   * @param source
   *          - Source airport
   * @param destination
   *          - Destination airport
   * 
   * @return Distance between two airports
   */
  public static Double getEuclideanDistance(Airport source, Airport destination) {
    if (source == null || destination == null) {
      return Double.MAX_VALUE;
    }

    return distance(source.getLatitude(), destination.getLatitude(), source.getLongitude(), destination.getLongitude(),
        source.getAltitude(), destination.getAltitude());
  }

  public static double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) {
    final int R = 6371;

    double latDistance = Math.toRadians(lat2 - lat1);
    double lonDistance = Math.toRadians(lon2 - lon1);
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
        * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = R * c;

    double height = el1 - el2;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance);
  }
}
