package com.htec.flightadvisor.algorithm;

import java.util.List;

import com.htec.flightadvisor.entity.Airport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * This class represents node of the priority queue used when finding multiple cheapest routes. It holds information
 * which airport it is, cost to it and path to it where path is consisted of route ids.
 */
public class AStarNodeWithPath {

  private Airport airport;

  private Double cost;

  private List<Long> routeIdsPath;
}
