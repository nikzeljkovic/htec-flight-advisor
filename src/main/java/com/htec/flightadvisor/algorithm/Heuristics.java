package com.htec.flightadvisor.algorithm;

import java.util.List;

import com.htec.flightadvisor.entity.Airport;

/**
 * Calculates heuristic from current airport to the destination city. Since destination city might have more than one
 * airport, we are calculating distance to the closest airport and then applying heuristic.
 */
public class Heuristics {

  public static Double multiplier;

  public static Double calculateHeuristic(Airport source, List<Airport> airportsInDestinationCity) {
    Double minDistance = Double.MAX_VALUE;

    for (Airport airport : airportsInDestinationCity) {
      Double tempDistance = GeographicalDistance.getEuclideanDistance(source, airport);

      minDistance = Math.min(minDistance, tempDistance);
    }

    return minDistance * multiplier;
  }
}
