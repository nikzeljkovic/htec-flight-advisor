package com.htec.flightadvisor.algorithm;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.assertj.core.util.Lists;
import org.assertj.core.util.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.htec.flightadvisor.dto.Flight;
import com.htec.flightadvisor.dto.FlightPlan;
import com.htec.flightadvisor.entity.Airport;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.entity.Route;
import com.htec.flightadvisor.exception.NoPathException;
import com.htec.flightadvisor.repository.AirportRepository;
import com.htec.flightadvisor.repository.RouteRepository;
import com.htec.flightadvisor.validator.db.CityDbValidator;

@Service
public class CheapestPathFinder {

  private static final Logger LOGGER = LoggerFactory.getLogger(CheapestPathFinder.class);

  private final AirportRepository airportRepository;

  private final RouteRepository routeRepository;

  private final CityDbValidator cityDbValidator;

  private final FlightPlanResponseBuilder flightPlanResponseBuilder;

  public CheapestPathFinder(AirportRepository airportRepository, RouteRepository routeRepository,
      CityDbValidator cityDbValidator, FlightPlanResponseBuilder flightPlanResponseBuilder) {
    this.airportRepository = airportRepository;
    this.routeRepository = routeRepository;
    this.cityDbValidator = cityDbValidator;
    this.flightPlanResponseBuilder = flightPlanResponseBuilder;
  }

  /**
   * Using A* algorithm, method will search for cheapest path between two nodes where node represents airport. Priority
   * queue is used to store airport and cost which represents how much money do we need to get to that airport. We will
   * evaluate only node which is first element in the queue because it has lowest price. Once first element is
   * destination city, we are calculating path and returning FlightPlan object which stores information about path (all
   * routes) and total cost and length.
   * 
   * @param sourceCityName
   *          - Name of the source city
   * @param destinationCityName
   *          - Name of the destination city
   *
   * @throws NoPathException
   *           - If there is no path between give source and destination city.
   * @return Shortest path between given source and destination city.
   */
  @Transactional(readOnly = true)
  public FlightPlan findShortestPath(String sourceCityName, String destinationCityName) {
    City source = cityDbValidator.validateCityExistsByName(sourceCityName);
    City destination = cityDbValidator.validateCityExistsByName(destinationCityName);

    List<Airport> airportsInSourceCity = airportRepository.findByCityId(source.getId());
    List<Airport> airportsInDestinationCity = airportRepository.findByCityId(destination.getId());
    if (CollectionUtils.isEmpty(airportsInSourceCity) || CollectionUtils.isEmpty(airportsInDestinationCity)) {
      throw new NoPathException(sourceCityName, destinationCityName);
    }

    Set<Long> visitedAirports = Sets.newHashSet();
    Map<Airport, Airport> cameFrom = Maps.newHashMap();
    AStarNodePriorityQueue queue = new AStarNodePriorityQueue(Comparator.comparingDouble(AStarNode::getCost));

    initQueueWithAirportsInSourceCity(queue, airportsInSourceCity);

    int nodesEvaluated = 0;
    while (!queue.isEmpty()) {
      AStarNode currentNode = queue.poll();
      Airport currentAirport = currentNode.getAirport();
      nodesEvaluated++;

      if (currentAirport.getCity().getId().equals(destination.getId())) {
        LOGGER.info("Nodes evaluated for searching '{}->{}': {} nodes. Cost of final node: {}.", source.getName(),
            destination.getName(), nodesEvaluated, currentNode.getCost());
        return flightPlanResponseBuilder.buildFlightPlan(cameFrom, source, currentAirport);
      }

      visitedAirports.add(currentAirport.getId());

      for (Airport neighbourAirport : getNeighbourAirports(currentAirport.getId())) {
        if (visitedAirports.contains(neighbourAirport.getId())) {
          continue;
        }
        Double cost =
            currentNode.getCost() + findShortestDirectRoutePrice(currentAirport.getId(), neighbourAirport.getId());

        if (queue.notContainsAirport(neighbourAirport.getId())) {
          queue.add(getAStarNode(neighbourAirport, cost));
        } else if (cost >= queue.getCost(neighbourAirport)) {
          continue;
        }

        cameFrom.put(neighbourAirport, currentAirport);
        queue.setCost(neighbourAirport, cost);
      }
    }
    throw new NoPathException(sourceCityName, destinationCityName);
  }

  private void initQueueWithAirportsInSourceCity(AStarNodePriorityQueue queue, List<Airport> airportsInSourceCity) {
    for (Airport airport : airportsInSourceCity) {
      queue.add(getAStarNode(airport, 0D));
    }
  }

  private Double heuristic(Airport airport, List<Airport> airportsInDestinationCity) {
    return Heuristics.calculateHeuristic(airport, airportsInDestinationCity);
  }

  private Double findShortestDirectRoutePrice(Long sourceAirportId, Long destinationAirportId) {
    return routeRepository
        .findFirstBySourceAirportIdAndDestinationAirportIdOrderByPriceAsc(sourceAirportId, destinationAirportId)
        .getPrice();
  }

  private List<Airport> getNeighbourAirports(Long currentAirport) {
    List<Route> routesToNeighbours = routeRepository.findBySourceAirportId(currentAirport);
    Set<Long> airportIds = routesToNeighbours.stream().map(Route::getDestinationAirportId).collect(Collectors.toSet());

    return airportRepository.findAllById(airportIds);
  }

  private AStarNode getAStarNode(Airport airport, Double cost) {
    return AStarNode.builder().airport(airport).cost(cost).build();
  }

  /**
   * Using A* algorithm, method will search for cheapest paths between two nodes where node represents airport. Priority
   * queue is used to store airport, cost which represents how much money do we need to get to that airport and path to
   * that node. We will evaluate only node which is first element in the queue because it has lowest price. Once first
   * element is destination city, we are calculating path, building FlightPlan object which stores information about
   * path (all routes), total cost and length, and adding it to all found paths list.
   *
   * @param sourceCityName
   *          - Name of the source city
   * @param destinationCityName
   *          - Name of the destination city
   * @param pathsCount
   *          - Number of requested cheapest paths
   *
   * @throws NoPathException
   *           - If there is no path between give source and destination city.
   * @return Shortest paths between given source and destination city.
   */
  @Transactional(readOnly = true)
  public List<FlightPlan> findShortestPaths(String sourceCityName, String destinationCityName, int pathsCount) {
    City source = cityDbValidator.validateCityExistsByName(sourceCityName);
    City destination = cityDbValidator.validateCityExistsByName(destinationCityName);

    List<Airport> airportsInSourceCity = airportRepository.findByCityId(source.getId());
    List<Airport> airportsInDestinationCity = airportRepository.findByCityId(destination.getId());
    if (CollectionUtils.isEmpty(airportsInSourceCity) || CollectionUtils.isEmpty(airportsInDestinationCity)) {
      throw new NoPathException(sourceCityName, destinationCityName);
    }

    PriorityQueue<AStarNodeWithPath> queue =
        new PriorityQueue<>(Comparator.comparingDouble(AStarNodeWithPath::getCost));

    initQueueWithAirportsInSourceCity(queue, airportsInSourceCity);

    int nodesEvaluated = 0;
    int pathsFound = 0;
    List<FlightPlan> paths = Lists.newArrayList();

    while (!queue.isEmpty() && pathsFound < pathsCount) {
      AStarNodeWithPath currentNode = queue.poll();
      Airport currentAirport = currentNode.getAirport();
      nodesEvaluated++;

      if (currentAirport.getCity().getId().equals(destination.getId())) {
        if (isUniquePath(paths, currentNode)) {
          paths.add(flightPlanResponseBuilder.buildFlightPlan(currentNode, source, currentAirport));
          pathsFound++;

          LOGGER.info("Nodes evaluated for searching '{}->{}': {} nodes. Route number: {}. Cost of final node: {}.",
              source.getName(), destination.getName(), nodesEvaluated, pathsFound, currentNode.getCost());

          if (pathsFound == pathsCount) {
            return paths;
          }
        }
      }

      for (Airport neighbourAirport : getNeighbourAirports(currentAirport.getId())) {
        List<Route> routes = findRoutesToNeighbour(currentAirport.getId(), neighbourAirport.getId());

        for (Route route : routes) {
          Double cost = currentNode.getCost() + route.getPrice();

          queue.add(getInitialAStarNodeWithPath(currentNode, neighbourAirport, route, cost));
        }
      }
    }

    if (pathsFound == 0) {
      throw new NoPathException(sourceCityName, destinationCityName);
    } else {
      return paths;
    }
  }

  private void initQueueWithAirportsInSourceCity(PriorityQueue<AStarNodeWithPath> queue,
      List<Airport> airportsInSourceCity) {
    for (Airport airport : airportsInSourceCity) {
      queue.add(getInitialAStarNodeWithPath(airport, Lists.newArrayList()));
    }
  }

  private AStarNodeWithPath getInitialAStarNodeWithPath(Airport airport, List<Long> path) {
    return AStarNodeWithPath.builder().airport(airport).cost(0D).routeIdsPath(path).build();
  }

  private List<Route> findRoutesToNeighbour(Long sourceAirportId, Long destinationAirportId) {
    return routeRepository.findBySourceAirportIdAndDestinationAirportId(sourceAirportId, destinationAirportId);
  }

  private boolean isUniquePath(List<FlightPlan> paths, AStarNodeWithPath currentNode) {
    boolean unique = true;

    for (FlightPlan flightPlan : paths) {
      List<Long> currentSolutionPath =
          flightPlan.getFlights().stream().map(Flight::getRouteId).collect(Collectors.toList());

      unique = unique && !currentSolutionPath.equals(currentNode.getRouteIdsPath());
    }

    return unique;
  }

  private AStarNodeWithPath getInitialAStarNodeWithPath(AStarNodeWithPath currentNode, Airport neighbourAirport,
      Route route, Double cost) {
    List<Long> currentPath = Lists.newArrayList(currentNode.getRouteIdsPath());
    currentPath.add(route.getId());

    return AStarNodeWithPath.builder().airport(neighbourAirport).cost(cost).routeIdsPath(currentPath).build();
  }
}
