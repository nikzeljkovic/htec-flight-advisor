package com.htec.flightadvisor.service;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.utils.JsonUtil;
import com.htec.flightadvisor.dto.AccountDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.entity.Account;
import com.htec.flightadvisor.entity.Role;
import com.htec.flightadvisor.repository.AccountRepository;
import com.htec.flightadvisor.validator.db.AccountDbValidator;

@Service
public class AccountService {

  private final AccountDbValidator accountDbValidator;

  private final JsonUtil jsonUtil;

  private final ModelMapper modelMapper;

  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  private final AccountRepository accountRepository;

  private static final Long DEFAULT_USER_ROLE_ID = 2L;

  public AccountService(AccountDbValidator accountDbValidator, JsonUtil jsonUtil, ModelMapper modelMapper,
      BCryptPasswordEncoder bCryptPasswordEncoder, AccountRepository accountRepository) {
    this.accountDbValidator = accountDbValidator;
    this.jsonUtil = jsonUtil;
    this.modelMapper = modelMapper;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    this.accountRepository = accountRepository;
  }

  @Transactional(readOnly = true)
  public AccountDto findByEmail(String email) {
    Account account = accountDbValidator.validateFindByEmailRequest(email);

    return jsonUtil.map(account);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DynamicResponse register(AccountDto accountDto) {
    accountDbValidator.validateRegistrationRequest(accountDto);

    return registerAccount(accountDto);
  }

  private DynamicResponse registerAccount(AccountDto accountDto) {
    Account account = modelMapper.map(accountDto, Account.class);

    setDefaultCreationValues(account);
    encodePassword(account);

    account = accountRepository.save(account);

    return new DynamicResponse(RestApiConstants.ID, account.getId());
  }

  private void setDefaultCreationValues(Account account) {
    account.setRoles(new ArrayList<>());
    account.getRoles().add(Role.builder().id(DEFAULT_USER_ROLE_ID).build());
  }

  private void encodePassword(Account account) {
    account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
  }
}
