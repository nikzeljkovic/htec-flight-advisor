package com.htec.flightadvisor.service;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.utils.JsonUtil;
import com.htec.flightadvisor.dto.CityDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.entity.City;
import com.htec.flightadvisor.repository.CityRepository;
import com.htec.flightadvisor.validator.db.CityDbValidator;

@Service
public class CityService {

  private final CityDbValidator cityDbValidator;

  private final CityRepository cityRepository;

  private final ModelMapper modelMapper;

  private final JsonUtil jsonUtil;

  public CityService(CityDbValidator cityDbValidator, CityRepository cityRepository, ModelMapper modelMapper,
      JsonUtil jsonUtil) {
    this.cityDbValidator = cityDbValidator;
    this.cityRepository = cityRepository;
    this.modelMapper = modelMapper;
    this.jsonUtil = jsonUtil;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DynamicResponse create(CityDto cityDto) {
    cityDbValidator.validateCreateRequest(cityDto);

    City city = modelMapper.map(cityDto, City.class);
    city = cityRepository.save(city);

    return new DynamicResponse(RestApiConstants.ID, city.getId());
  }

  @Transactional(readOnly = true)
  public Page<CityDto> findAll(String name, Integer commentsLimit, Pageable pageable) {
    return cityRepository.findByNameContainingIgnoreCase(name, pageable).map(city -> jsonUtil.map(city, commentsLimit));
  }
}
