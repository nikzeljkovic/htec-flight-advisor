package com.htec.flightadvisor.service;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.htec.flightadvisor.common.api.RestApiConstants;
import com.htec.flightadvisor.common.utils.RestApiUtils;
import com.htec.flightadvisor.dto.CommentDto;
import com.htec.flightadvisor.dto.DynamicResponse;
import com.htec.flightadvisor.entity.Comment;
import com.htec.flightadvisor.repository.CommentsRepository;
import com.htec.flightadvisor.validator.db.CommentDbValidator;

@Service
public class CommentsService {

  private final CommentsRepository commentsRepository;

  private final CommentDbValidator commentDbValidator;

  private final AccountService accountService;

  private final ModelMapper modelMapper;

  public CommentsService(CommentsRepository commentsRepository, CommentDbValidator commentDbValidator,
      AccountService accountService, ModelMapper modelMapper) {
    this.commentsRepository = commentsRepository;
    this.commentDbValidator = commentDbValidator;
    this.accountService = accountService;
    this.modelMapper = modelMapper;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public DynamicResponse create(CommentDto commentDto, String userEmail) {
    commentDbValidator.validateCreateRequest(commentDto);

    commentDto.setAccount(accountService.findByEmail(userEmail));

    Comment comment = modelMapper.map(commentDto, Comment.class);
    setDefaultValues(comment);
    comment = commentsRepository.save(comment);

    return new DynamicResponse(RestApiConstants.ID, comment.getId());
  }

  private void setDefaultValues(Comment comment) {
    comment.setCreatedAt(DateTime.now(DateTimeZone.UTC));
    comment.setUpdatedAt(null);
  }

  @Transactional
  public void deleteById(Long commentId, String requesterEmail) {
    Comment comment = commentDbValidator.validateDeleteRequest(commentId, requesterEmail);

    commentsRepository.delete(comment);
  }

  @Transactional
  public void update(CommentDto commentDto, String requesterEmail) {
    Comment existingComment = commentDbValidator.validateUpdateRequest(commentDto, requesterEmail);

    if (shouldUpdateComment(existingComment, commentDto)) {
      Comment updatedComment = modelMapper.map(commentDto, Comment.class);
      setNonUpdatableFieldsToNull(updatedComment);
      updatedComment.setUpdatedAt(DateTime.now(DateTimeZone.UTC));

      RestApiUtils.copyNonNullProperties(updatedComment, existingComment);

      commentsRepository.save(updatedComment);
    }
  }

  private void setNonUpdatableFieldsToNull(Comment updateComment) {
    updateComment.setCreatedAt(null);
    updateComment.setAccount(null);
    updateComment.setCity(null);
  }

  private boolean shouldUpdateComment(Comment comment, CommentDto commentDto) {
    return descriptionHasChanged(comment, commentDto);
  }

  private boolean descriptionHasChanged(Comment comment, CommentDto commentDto) {
    return !comment.getDescription().equals(commentDto.getDescription());
  }
}
