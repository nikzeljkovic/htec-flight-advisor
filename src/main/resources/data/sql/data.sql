INSERT INTO role (id_role, label, is_default) VALUES (1, 'ROLE_ADMINISTRATOR', FALSE);
INSERT INTO role (id_role, label, is_default) VALUES (2, 'ROLE_USER', TRUE);

INSERT INTO account (id_account, email, password, first_name, last_name) VALUES (1, 'admin@flight.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Admin', 'Admin');
INSERT INTO account_role (id_account, id_role) VALUES (1, 1);

INSERT INTO account (id_account, email, password, first_name, last_name) VALUES (2, 'registered@flight.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'User', 'User');
INSERT INTO account_role (id_account, id_role) VALUES (2, 2);

SET FOREIGN_KEY_CHECKS=0;
INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (1, 'Awesome city comm1.', '2018-01-30 00:00:00', NULL, 1604, 1);
INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (2, 'Awesome city comm2.', '2018-01-30 00:00:00', NULL, 1604, 1);
INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (3, 'Awesome city comm3.', '2018-01-30 00:00:00', NULL, 1604, 1);
INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (4, 'Awesome city comm4.', '2018-01-30 00:00:00', NULL, 1604, 1);
SET FOREIGN_KEY_CHECKS=1;