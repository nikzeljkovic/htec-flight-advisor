SET FOREIGN_KEY_CHECKS=0;

TRUNCATE TABLE `account_role`;
TRUNCATE TABLE `account`;
TRUNCATE TABLE `airport`;
TRUNCATE TABLE `city`;
TRUNCATE TABLE `comment`;
TRUNCATE TABLE `role`;
TRUNCATE TABLE `route`;

INSERT INTO role (id_role, label, is_default) VALUES (1, 'ROLE_ADMINISTRATOR', FALSE);
INSERT INTO role (id_role, label, is_default) VALUES (2, 'ROLE_USER', TRUE);

INSERT INTO account (id_account, email, password, first_name, last_name) VALUES (1, 'admin@flight.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Admin', 'Admin');
INSERT INTO account_role (id_account, id_role) VALUES (1, 1);

INSERT INTO account (id_account, email, password, first_name, last_name) VALUES (2, 'registered@flight.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'User', 'User');
INSERT INTO account_role (id_account, id_role) VALUES (2, 2);

INSERT INTO airport VALUES (340,364,'Germany','E','FRA','EDDF',50.0333333,8.5705556,'Frankfurt am Main International Airport','Europe/Berlin','OurAirports','1','airport',1),(1489,495,'Hungary','E','BUD','LHBP',47.436901092499994,19.255599975599996,'Budapest Ferenc Liszt International Airport','Europe/Budapest','OurAirports','1','airport',2),(1613,600,'Austria','E','VIE','LOWW',48.110298156738,16.569700241089,'Vienna International Airport','Europe/Vienna','OurAirports','1','airport',3),(1739,335,'Serbia','E','BEG','LYBE',44.8184013367,20.3090991974,'Belgrade Nikola Tesla Airport','Europe/Belgrade','OurAirports','1','airport',4);

INSERT INTO city VALUES (1,'Germany','Dummy city description.','Frankfurt'),(2,'Hungary','Dummy city description.','Budapest'),(3,'Austria','Dummy city description.','Vienna'),(4,'Serbia','Dummy city description.','Belgrade');

INSERT INTO route VALUES (1,'JU',19582,'','VIE',1613,'AT7',37.77,'BEG',1739,0),(2,'OS',491,'Y','VIE',1613,'DH4 F70 319 100 320',29.51,'BEG',1739,0),(3,'B2',1478,'','BUD',1489,'CRJ 735',44.31,'BEG',1739,0),(4,'JU',19582,'','BUD',1489,'AT7',71.9,'BEG',1739,0),(5,'ET',2220,'Y','FRA',340,'321 32A',91,'BUD',1489,0),(6,'LH',3320,'','FRA',340,'733 320 321 735 32A',31.04,'BUD',1489,0),(7,'SK',4319,'Y','FRA',340,'320 321',93.63,'BUD',1489,0),(8,'AB',214,'Y','FRA',340,'E90 320',21.86,'VIE',1613,0),(9,'ET',2220,'Y','FRA',340,'321',30.23,'VIE',1613,0),(10,'HG',3661,'','FRA',340,'E90 320',65.34,'VIE',1613,0),(11,'LH',3320,'','FRA',340,'321',53.89,'VIE',1613,0),(12,'OS',491,'Y','FRA',340,'321 320 319',99.09,'VIE',1613,0);

INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (1, 'Awesome city comment1.', '2018-01-30 00:00:00', NULL, 4, 1);
INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (2, 'Awesome city comment2.', '2018-01-30 00:00:00', NULL, 4, 1);
INSERT INTO comment (id_comment, description, created_at, updated_at, fk_id_city, fk_id_account) VALUES (3, 'Awesome city comment3.', '2018-01-30 00:00:00', NULL, 4, 1);

INSERT INTO city (city_name, description, country) VALUES ('CityWithoutAirports', 'Some description', 'Some Country');

SET FOREIGN_KEY_CHECKS=1;