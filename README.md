# Flight advisor
Project's purpose is to allow users to register an account and use application functionalities such as: creating a city, creating a comment for the city, 
updating and deleting a comment. But, main functionality is to find cheapest route between two given cities.

# Technology stack
Spring Boot and H2 database.

# Building project
In order to build the project, run ```mvn clean install``` in the project root folder.
If you want to build the project without running tests, please use ```mvn clean install -DskipTests``` in the project root folder.

# Start application
In order to start the application, run ```java -jar target/flight-advisor-0.0.1.jar``` in the project root folder.
Please consider that start time can be around 45-60 seconds (depening on machine) because it we need to load datasets (routes and airports) and store them in database.

# Algorithm
Algorithm used for solving problem is `A*` which is similar to `Dijkstra's` algorithm. Main difference is that A* has heuristic which reduce time and space complexity of the algorithm in most of the cases because it will turn us in right direction.

# Additional feature
There is possibility to return more than 1 cheapest flight. Due to performance limitations, you can choose between 1 and 5 cheapest flights.

# Applied optimizations
  - Database optimizations
  - Algorithm optimizations
  - Heuristic

# Possible optimizations
  - Second-level cache
  - Load datasets into memory (some HashMaps)

# API documentation

### Endpoints that are only exposed to registered users
For these endpoints, you should include `Authorization` header with format: `Bearer <token>`. For obtaining a token, please see `Invoke login`.

### User registration
POST: http://localhost:9100/api/v1/accounts
```javascript
{
	"email": "example@flight.com",
	"password": "password",
	"firstName": "Name", 
	"lastName": "Surname"
}
```

### Invoke login
http://localhost:9100/oauth/authorize?response_type=token&client_id=web-client&redirect_uri=http%3A%2F%2Flocalhost%3A9100%2F

Available users are `admin@flight.com` and `registered@flight.com`. For both users password is `password`.

### Retrieve logged user information
GET: http://localhost:9100/api/v1/account/me

### Add a city
POST: http://localhost:9100/api/v1/cities
```javascript
{
	"name": "Some city",
	"country": "Some country",
	"description": "Some description"
}
```

### Retrieve cities
GET: http://localhost:9100/api/v1/cites
Optional query params:
  - name
  - comments-limit
  - pageable params (page and size)

### Add a comment for the city
POST: http://localhost:9100/api/v1/comments
```javascript
{
	"description": "Comment description",
	"city": {
		"id": 1
	}
}
```

### Delete a comment
DELETE: http://localhost:9100/api/v1/comment/{id}

### Update a comment
PUT: http://localhost:9100/api/v1/comment
```javascript
{
	"id": "1",
	"descriotion": "Updated comment description"
}
```

### Find cheapest flight(s)
GET: localhost:9100/api/v1/routes
Required query params
  - source-city-name
  - destination-city-name

Optional query params
  - routes-limit (value should be between 1 and 5)